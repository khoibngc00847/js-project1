/*
* Sample Component 1 
* 
* NJ
* 27/4/2015
*/
var sampleComponent = function (options) {

    console.log( options );
    
    /* Tab ID, preview content and Container */
    this.tab = options.tab;
	this.container = options.container;
    this.content = options.content;
    
    /* initialize component generator */
    this.generator = new madCreator({
        id : 26
    });
    
    /* define the data structure if its empty */
     /* define the data structure if its empty */
    this.data = Object.keys(options.data).length != 0 ? options.data : {
        'type': 'page',
        'url': '',
        'showFriendsFaces': false,
        'adaptContainer': true,
        'hideCoverPhoto': false,
        'useSmallHeader': true,
        'messages': true,
        'events': true,
        'timeline': true,
        'width' : 0,
        'height' : 0
	};
    
    /* @NOTE set structure to pref */
    this.generator.setPrefStructure(this.data);
    
    this.render();
}

sampleComponent.prototype.render  = function () {
    
    /* used to replace this when it is not accessible in a function */
    var _this = this;
    
    /* Generate input text plugin tab */
    var plugin_text = this.generator.getInput({
        'title' : {
            'title' : 'Facebook Page Url',
			'class' : 'facebook-plugin',
        },
        'input' : {
            'type' : 'text',
            'value' : '',
			'placeholder': 'http://facebook/mobileads.com',
			'class' : 'facebook-plugin url'
        },
		'setPref' : {
            'type' : 'keyup',
            'target' : 'url'
        }
    });
    
    /* Generate checkboxes plugin tab */
    var plugin_checkboxes = this.generator.getCheckboxes({
        'input' : {
            'checkboxes' : [
                {
					'text' : ' Use Small Header', 
					'value' : 'useSmallHeader', 
					'class' : 'facebook-plugin facebook-plugin-checkboxes',
					'checked': 'checked'
				},
                {
					'text' : ' Hide Cover Photo', 
					'value' : 'hideCoverPhoto', 
					'class' : 'facebook-plugin facebook-plugin-checkboxes'
				},
				{
					'text' : " Show Friend's Face", 
					'value' : 'showFriendsFaces', 
					'class' : 'facebook-plugin facebook-plugin-checkboxes'
				}
            ]
        }
    });
    
    /* Generate radio buttons plugin tab */
    var plugin_radios = this.generator.getRadios({
        'input' : {
            'radio' : [
                {'text' : ' Page Plugin', 
				 'value' : 'page', 
				 'class' : 'page-plugin'
				},
                {'text' : ' Embedded Post', 
				 'value' : 'post', 
				  'class' : 'embedded-post'
				}
            ]
        },
        'event' : {
            'type' : 'click',
            'callback' : function () {
				var generator = new madCreator({
					id : 26
				});
				
				$( this ).closest( 'div.creator-tab' ).find( '.facebook-plugin' ).parent().css( 'display', 'none' );
				$( this ).closest( 'div.creator-tab' ).find( '.facebook-embedded' ).parent().css( 'display', 'none' );
				
				if( $( this ).val() == 'post' ) {
					$( this ).closest( 'div.creator-tab' ).find( '.facebook-embedded' ).parent().css( 'display', 'inline-block' );
					$( this ).closest( 'div.innerTinyDivision' ).find( 'input.page-plugin' )[0].checked = false;
					generator.setPref( 'url', $( this ).closest( 'div.creator-tab' ).find( 'input.facebook-embedded.url' ).val() );
				} else {
					$( this ).closest( 'div.creator-tab' ).find( '.facebook-plugin' ).parent().css( 'display', 'block' );
					$( this ).closest( 'div.innerTinyDivision' ).find( 'input.embedded-post' )[0].checked = false;
					generator.setPref( 'url', $( this ).closest( 'div.creator-tab' ).find( 'input.facebook-plugin.url' ).val() );
				}
            }
        },
		'setPref' : {
            'type' : 'click',
            'target' : 'type'
        }
    });
    
    /* Generate input text embedded tab*/
    var embedded_text1 = this.generator.getInput({
        'title' : {
            'title' : 'Facebook Post Url',
			'class' : 'facebook-embedded'
        },
        'input' : {
            'type' : 'text',
            'value' :'',
			'class' : 'facebook-embedded url'
        },
		'setPref' : {
            'type' : 'keyup',
            'target' : 'url'
        }
    });
	
	var embedded_text2 = this.generator.getInput({
        'title' : {
            'title' : 'Post Width(px)',
			'class' : 'facebook-embedded'
        },
        'input' : {
            'type' : 'text',
            'value' : '',
			'class' : 'facebook-embedded'
        },
		'setPref' : {
            'type' : 'keyup',
            'target' : 'width'
        }
    });
	
	var embedded_text3 = this.generator.getInput({
        'title' : {
            'title' : 'Post Height(px)',
			'class' : 'facebook-embedded'
        },
        'input' : {
            'type' : 'text',
            'value' : '',
			'class' : 'facebook-embedded'
        },
		'setPref' : {
            'type' : 'keyup',
            'target' : 'height'
        }
    });
    
    var section = this.generator.section({
        'title' : {
			'id' : 'facebook-plugin',
			'title': 'Facebook Settings',
        },
        'input' : [plugin_radios, plugin_text, plugin_checkboxes, embedded_text1, embedded_text2, embedded_text3],
        'expandable' : {
            'content' : 'show',
        }
    });
	
	$( section ).closest( 'div.innerSection' ).find( 'div.section' ).css({
		display: 'block',
		overflow: 'initial'
	});
	
	$( section ).find( '.facebook-embedded' ).parent().css( 'display', 'none' );
	
	$( plugin_radios ).closest( 'div.innerTinyDivision' ).children( 'div' ).css({
		float : 'left',
		marginLeft: '20px',
		marginBottom: '20px'
	});
	
	$( plugin_radios ).closest( 'div.innerTinyDivision' ).find( 'input.page-plugin' ).parent().css({
		marginLeft: '0px',
	});
	
	$( embedded_text2 ).css({
		width: '45%'
	})
	
	$( embedded_text3 ).css({
		width: '45%'
	})
	
	$( plugin_radios ).closest( 'div.innerTinyDivision' ).find( 'input.page-plugin' )[0].checked = true;
	
	this.container.append(section);
}