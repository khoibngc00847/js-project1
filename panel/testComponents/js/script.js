
    /*
    * Constructor
    */
    var LeadGen = function(options) {
        this.id = options.id;
        this.content = options.content;
        this.width = options.width;
        this.height = options.height;
        this.componentData = options.componentData;

        this.init();
    };

    /*
    * Helping method that wraps HTML elements with other HTML element
    */
    HTMLElement.prototype.wrap = function(elms) {
        // Convert `elms` to an array, if necessary.
        if (!elms.length) elms = [elms];

        // Loops backwards to prevent having to clone the wrapper on the
        // first element (see `child` below).
        for (var i = elms.length - 1; i >= 0; i--) {
            var child = (i > 0) ? this.cloneNode(true) : this;
            var el    = elms[i];

            // Cache the current parent and sibling.
            var parent  = el.parentNode;
            var sibling = el.nextSibling;

            // Wrap the element (is automatically removed from its current
            // parent).
            child.appendChild(el);

            // If the element had a sibling, insert the wrapper before
            // the sibling to maintain the HTML structure; otherwise, just
            // append it to the parent.
            if (sibling) {
                parent.insertBefore(child, sibling);
            } else {
                parent.appendChild(child);
            }
        }
    };

    /*
    * Finds y value of given object
    */
    function findPos(obj) {
        var curtop = 0;
        if (obj.offsetParent) {
            do {
                curtop += obj.offsetTop;
            } while (obj = obj.offsetParent);
            return curtop;
        }
    }

    /*
    * Initialize form creation
    */
    LeadGen.prototype.init = function() {


        this.containerElement = document.createElement('div')
        this.containerElement.id = 'leadgen-' + this.id;
        this.containerElement.className = 'leadgen';
        this.content.appendChild(this.containerElement)
        // apply styles to main container
        var wrapper = document.createElement("DIV");
        wrapper.setAttribute("class", "wrapper");
        wrapper.style.width = this.width + "px";
        wrapper.style.height = this.height + "px";
        wrapper.wrap(this.containerElement);
        this.containerElement.style.backgroundColor = this.componentData.themeStyle.bgColor;
        this.containerElement.style.color = this.componentData.themeStyle.textColor;
        this.containerElement.style.position = 'relative';
        this.containerElement.style.width = this.width + "px";
        this.containerElement.style.height = this.height + "px";

        var viewport = document.createElement("DIV");
        viewport.setAttribute("class", "content");
        viewport.style.backgroundColor = this.componentData.themeStyle.bgColor;
        viewport.style.color = this.componentData.themeStyle.textColor;
        this.containerElement.appendChild(viewport);

        // create FORM element inside main container
        var form = document.createElement("FORM");
        form.setAttribute("method", "GET");
        form.setAttribute("action", "http://mobileads.com/api/save_lf");
        viewport.appendChild(form);

        // dynamically create form elements according to componentData.leadGenEle array
        for (var i = 0; i < this.componentData.leadGenEle.length; i++) {
            this.inputFactory(form, this.componentData.leadGenEle[i].inputType, this.componentData.leadGenEle[i]);
        }

        // create submit button for the form
        var submitButton = document.createElement("INPUT");
        submitButton.setAttribute("type", "button");
        submitButton.setAttribute("value", this.componentData.submissionOption.submitButtonText);
        submitButton.style.backgroundColor = this.componentData.themeStyle.buttonColor;
        submitButton.style.color = this.componentData.themeStyle.buttonTextColor;
        submitButton.style.border = "1px solid #D2CCCC";
        submitButton.style.fontWeight = "bold";
        submitButton.style.float = "right";
        submitButton.style.cursor = "pointer";
        form.appendChild(submitButton);

        ssb.scrollbar(this.containerElement.id);

        var scrollCont = document.getElementsByClassName("ssb_st")[0];
        scrollCont.style.backgroundColor = this.componentData.themeStyle.bgColor;
        var scrollPos = document.getElementsByClassName("ssb_sb")[0];
        scrollPos.style.border = "1px solid #000000";
        scrollPos.style.borderRight = "none";
        scrollPos.style.backgroundColor = "#ffffff";
        scrollPos.style.display = "none";

        scrollPos.style.display = "block";

        // submitting the form
        var that = this;
        submitButton.addEventListener("click", function(event) {
            console.log('Submitting the form');
            var evt = event || window.event;
            evt.target = evt.target || evt.srcElement || null;
            var checkboxes = [];
            var field;
            var query = 'http://mobileads.com/api/save_lf';

            // if there are emails set in configuration, then pass the to the URL and GET parameter
            if (that.componentData.submissionOption.emailSend.enable == '1') {
                query += "?contactEmail=" + that.componentData.submissionOption.emailSend.emails;
                query += '&gotDatas=1&element=[';
            } else {
                query += '?gotDatas=1&element=[';
            }
            // parsing the form data and preparing URL to send GET request to the server via JSONP
            if (typeof form == 'object' && form.nodeName == "FORM") {
                for (var i = 0; i < form.elements.length; i++) {
                    field = form.elements[i];
                    if (field.name && field.type != 'file' && field.type != 'reset') {
                        if ((field.type != 'submit' && field.type != 'button') || evt.target == field) {
                            if ((field.type != 'checkbox' && field.type != 'radio') || (field.type == 'radio' && field.checked)) {
                                // validation here
                                function isInt(value){
                                    return /^-?\d+$/.test(String(value)); // validating numeric field
                                }
                                var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/; // regex pattern that validates if user entered valid email address
                                var datePattern = /^\d{4}\/\d{2}\/\d{2}$/; // regex pattern that validates if user entered valid date in format 'YYYY/MM/DD'

                                // check if filed is required and is not empty
                                if (field.required == true && field.value == '') {
                                    console.log("empty field");
                                    // adding validation for required fields
                                    if (field.type == "text" && field.id == "input-type-text") {
                                        var error = document.getElementById("text-validation-error");
                                        error.innerHTML = "This field is required.";
                                        error.innerText = "This field is required.";
                                    } else if (field.type == "email") {
                                        var error = document.getElementById("email-validation-error");
                                        error.innerHTML = "This field is required.";
                                        error.innerText = "This field is required.";
                                    } else if (field.type == "text" && field.id == "input-type-numeric") {
                                        var error = document.getElementById("numeric-validation-error");
                                        error.innerHTML = "This field is required.";
                                        error.innerText = "This field is required.";
                                    } else if (field.type == "textarea") {
                                        var error = document.getElementById("textarea-validation-error");
                                        error.innerHTML = "This field is required.";
                                        error.innerText = "This field is required.";
                                    } else if (field.type == "text" && field.id == "input-type-combo") {
                                        var error = document.getElementById("combo-validation-error");
                                        error.innerHTML = "This field is required.";
                                        error.innerText = "This field is required.";
                                    } else if (field.type == "text" && field.id == "input-type-date") {
                                        var error = document.getElementById("date-validation-error");
                                        error.innerHTML = "This field is required.";
                                        error.innerText = "This field is required.";
                                    }
                                    field.style.border = "1px solid red";
                                    field.style.boxShadow = "red 1px 1px 10px";
                                    that.containerElement.scrollTop = findPos(field) - 35;
                                    return false;
                                } else {
                                    var errorMsg = document.getElementsByClassName("validation-error");
                                    for (var el = 0; el < errorMsg.length; el++) {
                                        errorMsg[el].innerHTML = "";
                                        errorMsg[el].innerText = "";
                                    }
                                    // check if user entered valid number - integer
                                    if (field.value != '' && field.type == "text" && field.name == "input-type-numeric" && !isInt(field.value)) {
                                        console.log("wrong number field");
                                        // adding validation for number field
                                        field.style.border = "1px solid red";
                                        field.style.boxShadow = "red 1px 1px 10px";
                                        field.value = "";
                                        for (var k = 0; k < that.componentData.leadGenEle.length; k++) {
                                            if (that.componentData.leadGenEle[k].inputType == "numeric") {
                                                field.setAttribute("placeholder", that.componentData.leadGenEle[k].inputValue);
                                            }
                                        }

                                        var error = document.getElementById("numeric-validation-error");
                                        error.innerHTML = "Please enter valid phone number.";
                                        error.innerText = "Please enter valid phone number.";
                                        that.containerElement.scrollTop = findPos(field) - 35;
                                        return false;
                                    }
                                    // check if user entered valid date in format 'YYYY/MM/DD'
                                    if (field.value != '' && field.type == "text" && field.name == "input-type-date" && !datePattern.test(field.value)) {
                                        console.log("wrong date field");
                                        // adding validation for date field
                                        field.style.border = "1px solid red";
                                        field.style.boxShadow = "red 1px 1px 10px";
                                        field.value = "";
                                        for (var j = 0; j < that.componentData.leadGenEle.length; j++) {
                                            if (that.componentData.leadGenEle[j].inputType == "date") {
                                                field.setAttribute("placeholder", that.componentData.leadGenEle[j].inputValue);
                                            }
                                        }

                                        var error = document.getElementById("date-validation-error");
                                        error.innerHTML = "Please enter valid date in format 'YYYY/MM/DD'.";
                                        error.innerText = "Please enter valid date in format 'YYYY/MM/DD'.";
                                        that.containerElement.scrollTop = findPos(field) - 35;
                                        return false;
                                    }
                                    // check if user entered valid email address
                                    if (field.value != '' && field.type == "email" && !emailPattern.test(field.value)) {
                                        console.log("wrong email field");
                                        // adding validation for email field
                                        field.style.border = "1px solid red";
                                        field.style.boxShadow = "red 1px 1px 10px";
                                        field.value = "";
                                        for (var m = 0; m < that.componentData.leadGenEle.length; m++) {
                                            if (that.componentData.leadGenEle[m].inputType == "email") {
                                                field.setAttribute("placeholder", that.componentData.leadGenEle[m].inputValue);
                                            }
                                        }

                                        var error = document.getElementById("email-validation-error");
                                        error.innerHTML = "Please enter valid email address in format 'some@example.com'.";
                                        error.innerText = "Please enter valid email address in format 'some@example.com'.";
                                        that.containerElement.scrollTop = findPos(field) - 35;
                                        return false;
                                    }
                                }
                                // here we are sure that client side validation is passed

                                var errorMsg = document.getElementsByClassName("validation-error");
                                for (var el = 0; el < errorMsg.length; el++) {
                                    errorMsg[el].innerHTML = "";
                                    errorMsg[el].innerText = "";
                                }

                                field.style.border = "2px solid #20b571";
                                field.style.boxShadow = "none";
                                query += '{"fieldname":"' + field.name + '","value":"' + encodeURIComponent(field.value) + '"},';
                            } else if ((field.type == 'checkbox') && field.checked) {
                                // here we are adding all the checked checkboxes to an array
                                // to be able to marge them to one value
                                checkboxes.push(field);
                            }
                        }
                    }
                }
            }
            var part = '';
            // merging checkboxes' values into one value
            if (checkboxes.length != 0) {
                part += '{"fieldname":"' + checkboxes[0].name + '","value":"';
                for (var i = 0; i < checkboxes.length; i++) {
                    if (i != checkboxes.length - 1) {
                        part += encodeURIComponent(checkboxes[i].value) + ',';
                    } else {
                        part += encodeURIComponent(checkboxes[i].value);
                    }
                }
                part += '"}';
            } else {
                query = query.substring(0, query.length - 1);
            }
            query += part + ']';
            // adding submission parameters
            var sbmObj = that.componentData.submissionParam;
            query += '&user-id=' + encodeURIComponent(sbmObj.userId) + '&studio-id=' + encodeURIComponent(sbmObj.studioId) + '&tab-id=' + encodeURIComponent(sbmObj.tabId) + '&trackid=' + encodeURIComponent(sbmObj.trackId) + '&referredURL=' + encodeURIComponent(sbmObj.referredURL);
            var randomId = Math.floor((Math.random() * 10000000000000) + 1);
            // adding callback parameter for JSONP request
            query += '&callback=leadGenCallback&callback=leadGenCallback&_=' + randomId;

            // helping method that sends JSONP request
            function sendJsonpRequest(url, callback) {
                var callbackName = 'leadGenCallback';
                window[callbackName] = function(data) {
                    delete window[callbackName];
                    document.body.removeChild(script);
                    callback(data);
                };

                var script = document.createElement('script');
                script.src = url;
                document.body.appendChild(script);
            }

            sendJsonpRequest(query, leadGenCallback);

            // method that handles JSONP response
            function leadGenCallback(data) {
                console.log('Request is sent successfully');
                // if response was successful show popup with message
                if (data.status == true) {
                    console.log('Got successful response');
                    createSuccessPopup();
                }
            }

            // helping method that creates popup tiwh message on successful JSONP response
            function createSuccessPopup() {
                var comboOverlay = document.createElement("DIV");
                var popup = document.createElement("DIV");
                var popupContent = document.createElement("DIV");

                var scrollCont = document.getElementsByClassName("ssb_st")[0];
                var scroll = document.getElementsByClassName("ssb_sb")[0];
                if (scroll && scrollCont) {
                    scrollCont.style.display = "none";
                    scroll.style.display = "none";
                }

                comboOverlay.setAttribute("id", "overlay");
                comboOverlay.setAttribute("class", "overlay");
                popup.setAttribute("class", "success-popup");
                popup.style.backgroundColor = that.componentData.themeStyle.bgColor;
                popup.style.color = that.componentData.themeStyle.textColor;
                comboOverlay.appendChild(popup);
                popupContent.setAttribute("class", "popup-content");
                popupContent.innerHTML = that.componentData.successMessage;
                popupContent.innerText = that.componentData.successMessage;
                var closeButton = document.createElement("A");
                closeButton.style.color = that.componentData.themeStyle.textColor;
                closeButton.setAttribute("href", "#");
                closeButton.setAttribute("class", "close");
                closeButton.innerHTML = closeButton.innerText = 'x';
                popupContent.appendChild(closeButton);
                popup.appendChild(popupContent);
                that.containerElement.scrollTop = 0;
                that.containerElement.style.overflow = "hidden";
                that.containerElement.appendChild(comboOverlay);
                popupContent.style.paddingTop = (that.containerElement.offsetHeight - popupContent.offsetHeight) / 2 + "px";
                closeButton.addEventListener('click', function() {
                    that.containerElement.removeChild(comboOverlay);
                    that.containerElement.style.overflow = "auto";
                    scrollCont.style.display = "block";
                    scroll.style.display = "block";
                }, false);
            }
        }, false);
    };

    /*
    * Factory method that dynamically creates all the form fields based on configuration
    */
    LeadGen.prototype.inputFactory = function(parent, inputType, inputObjInfo) {
        switch (inputType) {
            case "freetext":
                this.createPureTextElement(parent, inputObjInfo);
                break;
            case "text":
                this.createTextElement(parent, inputObjInfo);
                break;
            case "email":
                this.createEmailElement(parent, inputObjInfo);
                break;
            case "numeric":
                this.createNumericElement(parent, inputObjInfo);
                break;
            case "combo":
                this.createComboElement(parent, inputObjInfo);
                break;
            case "textarea":
                this.createTextAreaElement(parent, inputObjInfo);
                break;
            case "radio":
                this.createRadioElement(parent, inputObjInfo);
                break;
            case "checkbox":
                this.createCheckboxElement(parent, inputObjInfo);
                break;
            case "date":
                this.createDateElement(parent, inputObjInfo);
                break;
            case "link":
                this.createLinkElement(parent, inputObjInfo);
                break;
        }
    };

    /*
    * Method that creates input of type 'freetext'. Really, it is styled "P" element with text
    */
    LeadGen.prototype.createPureTextElement = function(parent, objInfo) {
        var pElement = document.createElement("P");
        // appending text from componentData.leadGenEle.inputValue
        pElement.innerHTML = objInfo.inputValue;
        pElement.innerText = objInfo.inputValue;
        pElement.setAttribute("class", "input-style");
        pElement.style.color = this.componentData.themeStyle.textColor;
        pElement.style.boxSizing = "border-box";
        // append new element to the parent
        parent.appendChild(pElement);
    };

    /*
     * Method that creates input of type 'text'.
     */
    LeadGen.prototype.createTextElement = function(parent, objInfo) {
        var inputElement = document.createElement("INPUT");
        var labelElement = document.createElement("LABEL");
        var idAttr = "input-type-text";
        var brElement = document.createElement("BR");
        var errorMsg = document.createElement("P");

        inputElement.setAttribute("type", "text");
        inputElement.setAttribute("placeholder", objInfo.inputValue);
        inputElement.setAttribute("id", idAttr);
        inputElement.setAttribute("name", idAttr);
        inputElement.style.color = this.componentData.themeStyle.textColor;
        inputElement.style.backgroundColor = this.componentData.themeStyle.inputBgColor;
        inputElement.style.boxSizing = "border-box";

        labelElement.style.color = this.componentData.themeStyle.labelColor;
        labelElement.setAttribute("class", "input-style");
        labelElement.setAttribute("for", idAttr);
        labelElement.innerHTML = objInfo.inputLabel;
        labelElement.innerText = objInfo.inputLabel;

        if (objInfo.inputRequired == '1') {
            inputElement.required = true;
            labelElement.innerHTML = objInfo.inputLabel + " (*)";
            labelElement.innerText = objInfo.inputLabel + " (*)";
        }

        errorMsg.setAttribute("id", "text-validation-error");
        errorMsg.setAttribute("class", "validation-error");

        parent.appendChild(labelElement);
        parent.appendChild(brElement);
        parent.appendChild(inputElement);
        parent.appendChild(errorMsg);
    };

    /*
     * Method that creates input of type 'email'.
     */
    LeadGen.prototype.createEmailElement = function(parent, objInfo) {
        var inputElement = document.createElement("INPUT");
        var labelElement = document.createElement("LABEL");
        var idAttr = "input-type-email";
        var brElement = document.createElement("BR");
        var errorMsg = document.createElement("P");

        inputElement.setAttribute("type", "email");
        inputElement.setAttribute("placeholder", objInfo.inputValue);
        inputElement.setAttribute("id", idAttr);
        inputElement.setAttribute("name", idAttr);
        inputElement.style.color = this.componentData.themeStyle.textColor;
        inputElement.style.backgroundColor = this.componentData.themeStyle.inputBgColor;
        inputElement.style.boxSizing = "border-box";

        labelElement.style.color = this.componentData.themeStyle.labelColor;
        labelElement.setAttribute("class", "input-style");
        labelElement.setAttribute("for", idAttr);
        labelElement.innerHTML = objInfo.inputLabel;
        labelElement.innerText = objInfo.inputLabel;

        if (objInfo.inputRequired == '1') {
            inputElement.required = true;
            labelElement.innerHTML = objInfo.inputLabel + " (*)";
            labelElement.innerText = objInfo.inputLabel + " (*)";
        }

        errorMsg.setAttribute("id", "email-validation-error");
        errorMsg.setAttribute("class", "validation-error");

        parent.appendChild(labelElement);
        parent.appendChild(brElement);
        parent.appendChild(inputElement);
        parent.appendChild(errorMsg);
    };

    /*
     * Method that creates input of type 'numeric'. Really, it is input type text element with proper validation
      * that allow only integer numbers
     */
    LeadGen.prototype.createNumericElement = function(parent, objInfo) {
        var inputElement = document.createElement("INPUT");
        var labelElement = document.createElement("LABEL");
        var idAttr = "input-type-numeric";
        var brElement = document.createElement("BR");
        var errorMsg = document.createElement("P");

        inputElement.setAttribute("type", "text");
        inputElement.setAttribute("placeholder", objInfo.inputValue);
        inputElement.setAttribute("id", idAttr);
        inputElement.setAttribute("name", idAttr);
        inputElement.style.color = this.componentData.themeStyle.textColor;
        inputElement.style.backgroundColor = this.componentData.themeStyle.inputBgColor;
        inputElement.style.boxSizing = "border-box";

        labelElement.style.color = this.componentData.themeStyle.labelColor;
        labelElement.setAttribute("class", "input-style");
        labelElement.setAttribute("for", idAttr);
        labelElement.innerHTML = objInfo.inputLabel;
        labelElement.innerText = objInfo.inputLabel;

        if (objInfo.inputRequired == '1') {
            inputElement.required = true;
            labelElement.innerHTML = objInfo.inputLabel + " (*)";
            labelElement.innerText = objInfo.inputLabel + " (*)";
        }

        errorMsg.setAttribute("id", "numeric-validation-error");
        errorMsg.setAttribute("class", "validation-error");

        parent.appendChild(labelElement);
        parent.appendChild(brElement);
        parent.appendChild(inputElement);
        parent.appendChild(errorMsg);
    };

    /*
     * Method that creates input of type 'textarea'. Really, it is textarea element with text
     */
    LeadGen.prototype.createTextAreaElement = function(parent, objInfo) {
        var inputElement = document.createElement("TEXTAREA");
        var labelElement = document.createElement("LABEL");
        var idAttr = "input-type-textarea";
        var brElement = document.createElement("BR");
        var errorMsg = document.createElement("P");

        inputElement.setAttribute("type", "textarea");
        inputElement.setAttribute("placeholder", objInfo.inputValue);
        inputElement.setAttribute("id", idAttr);
        inputElement.style.color = this.componentData.themeStyle.textColor;
        inputElement.style.backgroundColor = this.componentData.themeStyle.inputBgColor;
        inputElement.style.boxSizing = "border-box";

        labelElement.style.color = this.componentData.themeStyle.labelColor;
        labelElement.setAttribute("class", "input-style");
        labelElement.setAttribute("for", idAttr);
        inputElement.setAttribute("name", idAttr);
        labelElement.innerHTML = objInfo.inputLabel;
        labelElement.innerText = objInfo.inputLabel;

        if (objInfo.inputRequired == '1') {
            inputElement.required = true;
            labelElement.innerHTML = objInfo.inputLabel + " (*)";
            labelElement.innerText = objInfo.inputLabel + " (*)";
        }

        errorMsg.setAttribute("id", "textarea-validation-error");
        errorMsg.setAttribute("class", "validation-error");

        parent.appendChild(labelElement);
        parent.appendChild(brElement);
        parent.appendChild(inputElement);
        parent.appendChild(errorMsg);
    };

    /*
     * Method that creates input of type 'radio'.
     */
    LeadGen.prototype.createRadioElement = function(parent, objInfo) {
        var radioValuesArr = objInfo.inputValue.split(",");
        var labelElement = document.createElement("LABEL");
        var brElement1 = document.createElement("BR");

        labelElement.style.color = this.componentData.themeStyle.labelColor;
        labelElement.innerHTML = objInfo.inputLabel;
        labelElement.innerText = objInfo.inputLabel;

        if (objInfo.inputRequired == '1') {
            labelElement.innerHTML = objInfo.inputLabel + " (*)";
            labelElement.innerText = objInfo.inputLabel + " (*)";
        }
        parent.appendChild(labelElement);
        parent.appendChild(brElement1);

        for (var i = 0; i < radioValuesArr.length; i++) {
            var radioWrapper = document.createElement("P");
            var inputElement = document.createElement("INPUT");
            var idAttr = "input-type-radio-" + (i + 1);

            inputElement.setAttribute("type", "radio");
            inputElement.setAttribute("id", idAttr);
            inputElement.setAttribute("name", "input-type-radio");
            inputElement.setAttribute("value", radioValuesArr[i]);

            if (i == 0) {
                inputElement.checked = true;
            }

            if (objInfo.inputRequired == '1') {
                inputElement.required = true;
            }

            var spanElement = document.createElement("SPAN");
            spanElement.innerHTML = radioValuesArr[i];
            spanElement.innerText = radioValuesArr[i];
//            spanElement.style.fontSize = "15px";
            spanElement.style.color = this.componentData.themeStyle.labelColor;

            radioWrapper.setAttribute("class", "input-radio-wrapper");
            radioWrapper.appendChild(inputElement);
            radioWrapper.appendChild(spanElement);
            parent.appendChild(radioWrapper);
        }

        var brElement2 = document.createElement("BR");
        brElement2.style.clear = "both";
        parent.appendChild(brElement2);
    };

    /*
     * Method that creates input of type 'checkbox'.
     */
    LeadGen.prototype.createCheckboxElement = function(parent, objInfo) {
        var radioValuesArr = objInfo.inputValue.split(",");
        var labelElement = document.createElement("LABEL");
        var brElement1 = document.createElement("BR");

        labelElement.style.color = this.componentData.themeStyle.labelColor;
        labelElement.innerHTML = objInfo.inputLabel;
        labelElement.innerText = objInfo.inputLabel;

        if (objInfo.inputRequired == '1') {
            labelElement.innerHTML = objInfo.inputLabel + " (*)";
            labelElement.innerText = objInfo.inputLabel + " (*)";
        }
        parent.appendChild(labelElement);
        parent.appendChild(brElement1);

        for (var i = 0; i < radioValuesArr.length; i++) {
            var radioWrapper = document.createElement("P");
            var inputElement = document.createElement("INPUT");
            var idAttr = "input-type-checkbox-" + (i + 1);

            inputElement.setAttribute("type", "checkbox");
            inputElement.setAttribute("id", idAttr);
            inputElement.setAttribute("name", "input-type-checkbox");
            inputElement.setAttribute("value", radioValuesArr[i]);

//            if (i == 0) {
//                inputElement.checked = true;
//            }

            if (objInfo.inputRequired == '1') {
                inputElement.required = true;
            }

            var spanElement = document.createElement("SPAN");
            spanElement.innerHTML = radioValuesArr[i];
            spanElement.innerText = radioValuesArr[i];
//            spanElement.style.fontSize = "15px";
            spanElement.style.color = this.componentData.themeStyle.labelColor;

            radioWrapper.setAttribute("class", "input-radio-wrapper");
            radioWrapper.appendChild(inputElement);
            radioWrapper.appendChild(spanElement);
            parent.appendChild(radioWrapper);
        }

        var brElement2 = document.createElement("BR");
        brElement2.style.clear = "both";
        parent.appendChild(brElement2);
    };

    /*
     * Method that creates input of type 'link'. Really, it is styled "A" element
     */
    LeadGen.prototype.createLinkElement = function(parent, objInfo) {
        var pElement = document.createElement("P");
        var linkElement = document.createElement("A");

        linkElement.setAttribute("href", objInfo.inputValue);
        linkElement.setAttribute("target", "_blank");
//        linkElement.style.fontSize = "12px";
        linkElement.innerHTML = objInfo.inputLabel;
        linkElement.innerText = objInfo.inputLabel;

        pElement.setAttribute("class", "input-style");
        pElement.style.boxSizing = "border-box";
        pElement.appendChild(linkElement);
        // append new element to the parent
        parent.appendChild(pElement);
    };


    /*
     * Method that creates input of type 'combo'. Really, it is intup type text element
      * when we click on it popup with overlay is created
      * popup content is e styled "UL" element with "LI" elements as childs
      * when we click on some "LI" it's value sets as value of input type text element
     */
    LeadGen.prototype.createComboElement = function(parent, objInfo) {
        var inputElement = document.createElement("INPUT");
        var labelElement = document.createElement("LABEL");
        var idAttr = "input-type-combo";
        var brElement = document.createElement("BR");
        var errorMsg = document.createElement("P");

        inputElement.setAttribute("type", "text");
        inputElement.setAttribute("placeholder", objInfo.inputLabel);
        inputElement.setAttribute("id", idAttr);
        inputElement.setAttribute("name", idAttr);
        inputElement.style.color = this.componentData.themeStyle.textColor;
        inputElement.style.backgroundColor = this.componentData.themeStyle.inputBgColor;
        inputElement.style.boxSizing = "border-box";

        labelElement.style.color = this.componentData.themeStyle.labelColor;
        labelElement.setAttribute("class", "input-style");
        labelElement.setAttribute("for", idAttr);
        labelElement.innerHTML = objInfo.inputLabel;
        labelElement.innerText = objInfo.inputLabel;

        if (objInfo.inputRequired == '1') {
            inputElement.required = true;
            labelElement.innerHTML = objInfo.inputLabel + " (*)";
            labelElement.innerText = objInfo.inputLabel + " (*)";
        }

        errorMsg.setAttribute("id", "combo-validation-error");
        errorMsg.setAttribute("class", "validation-error");

        parent.appendChild(labelElement);
        parent.appendChild(brElement);
        parent.appendChild(inputElement);
        parent.appendChild(errorMsg);

        inputElement.addEventListener("click", createCustomComboBox, false);

        var that = this;
        function createCustomComboBox() {
            var comboOverlay = document.createElement("DIV");
            var popup = document.createElement("DIV");
            var popupContent = document.createElement("DIV");

            var scrollCont = document.getElementsByClassName("ssb_st")[0];
            var scroll = document.getElementsByClassName("ssb_sb")[0];
            if (scroll && scrollCont) {
                scrollCont.style.display = "none";
                scroll.style.display = "none !important";
            }

            comboOverlay.setAttribute("id", "overlay");
            comboOverlay.setAttribute("class", "overlay");
            popup.setAttribute("class", "popup");
            popup.style.width = (that.containerElement.offsetWidth - 60) + "px";
            popup.style.height = (that.containerElement.offsetHeight - 60) + "px";
            comboOverlay.appendChild(popup);
            popupContent.setAttribute("class", "popup-content");
            popupContent.setAttribute("id", "combo-content");
            popup.appendChild(popupContent);
            that.containerElement.scrollTop = 0;
            that.containerElement.style.overflow = "hidden";
            that.containerElement.appendChild(comboOverlay);

            var comboUlElement = document.createElement("UL");
            var countriesArr = objInfo.inputValue.split(",");
            comboUlElement.setAttribute("class", "countries-ul");
            for (var i = 0; i < countriesArr.length; i++) {
                var countryLiElement = document.createElement("LI");
                countryLiElement.innerHTML = countriesArr[i];
                countryLiElement.innerText = countriesArr[i];
                countryLiElement.setAttribute("class", "country-li");
                comboUlElement.appendChild(countryLiElement);

                countryLiElement.addEventListener("click", updateComboBoxValue, false);
            }
            popupContent.appendChild(comboUlElement);

            ssb.scrollbar("combo-content");
            var scroll1 = document.getElementsByClassName("ssb_sb")[0];
            popup.addEventListener("mouseover", function() {
                scroll1.style.display = "block";
            }, false);
            popup.addEventListener("mouseout", function() {
                scroll1.style.display = "none";
            }, false);

            function updateComboBoxValue() {
                var selectedValue;

                scrollCont.style.display = "block";
                scroll.style.display = "block";

                if (this.innerHTML) {
                    selectedValue = this.innerHTML;
                } else {
                    selectedValue = this.innerText;
                }

                inputElement.setAttribute("value", selectedValue);
                that.containerElement.removeChild(comboOverlay);
                that.containerElement.style.overflow = "auto";
                that.containerElement.scrollTop = findPos(inputElement) - 35;
            }
        }
    };

    /*
     * Method that creates input of type 'date'. Really, it is input type text element
      * when we click on it popup with overlay is created
      * popup content is a datepicker
      * when we click on some date this value is set to the value of input type text
     */
    LeadGen.prototype.createDateElement = function(parent, objInfo) {
        var inputElement = document.createElement("INPUT");
        var labelElement = document.createElement("LABEL");
        var idAttr = "input-type-date";
        var brElement = document.createElement("BR");
        var errorMsg = document.createElement("P");

        inputElement.setAttribute("type", "text");
        inputElement.setAttribute("placeholder", objInfo.inputValue);
        inputElement.setAttribute("id", idAttr);
        inputElement.setAttribute("name", idAttr);
        inputElement.style.color = this.componentData.themeStyle.textColor;
        inputElement.style.backgroundColor = this.componentData.themeStyle.inputBgColor;
        inputElement.style.boxSizing = "border-box";

        labelElement.style.color = this.componentData.themeStyle.labelColor;
        labelElement.setAttribute("class", "input-style");
        labelElement.setAttribute("for", idAttr);
        labelElement.innerHTML = objInfo.inputLabel;
        labelElement.innerText = objInfo.inputLabel;

        if (objInfo.inputRequired == '1') {
            inputElement.required = true;
            labelElement.innerHTML = objInfo.inputLabel + " (*)";
            labelElement.innerText = objInfo.inputLabel + " (*)";
        }

        errorMsg.setAttribute("id", "date-validation-error");
        errorMsg.setAttribute("class", "validation-error");

        parent.appendChild(labelElement);
        parent.appendChild(brElement);
        parent.appendChild(inputElement);
        parent.appendChild(errorMsg);

        inputElement.addEventListener("click", createCustomDatePicker, false);

        var that = this;
        function createCustomDatePicker() {
            var comboOverlay = document.createElement("DIV");
            var popup = document.createElement("DIV");
            var popupContent = document.createElement("DIV");

            var scrollCont = document.getElementsByClassName("ssb_st")[0];
            var scroll = document.getElementsByClassName("ssb_sb")[0];
            if (scroll && scrollCont) {
                scrollCont.style.display = "none";
                scroll.style.display = "none";
            }

            comboOverlay.setAttribute("id", "overlay");
            comboOverlay.setAttribute("class", "overlay");
            popup.setAttribute("class", "popup-date");
//            popup.style.marginLeft = parseInt((that.containerElement.offsetWidth - 242) / 2) + "px";
//            popup.style.marginTop = parseInt((that.containerElement.offsetHeight - 210) / 2) + "px";
            comboOverlay.appendChild(popup);
            popupContent.setAttribute("class", "popup-content");
            popup.appendChild(popupContent);
            that.containerElement.scrollTop = 0;
            that.containerElement.style.overflow = "hidden";
            that.containerElement.appendChild(comboOverlay);

            var picker = new Pikaday({
                onSelect: function(date) {
                    var currentDate = new Date(picker.toString());
                    var curr_date = currentDate.getDate();
                    var curr_month = currentDate.getMonth() + 1; //Months are zero based
                    var curr_year = currentDate.getFullYear();

                    scrollCont.style.display = "block";
                    scroll.style.display = "block";

                    if (curr_date.toString().length == 1) {
                        curr_date = '0' + curr_date;
                    }
                    if (curr_month.toString().length == 1) {
                        curr_month = '0' + curr_month;
                    }

                    inputElement.value = curr_year + '/' + curr_month + '/' + curr_date;
                    that.containerElement.removeChild(comboOverlay);
                    that.containerElement.style.overflow = "auto";
                    that.containerElement.scrollTop = findPos(inputElement) - 35;
                }
            });
            popupContent.parentNode.insertBefore(picker.el, popupContent.nextSibling);

        }
    };

    
