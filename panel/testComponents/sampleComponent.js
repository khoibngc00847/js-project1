var sampleComponent = function (options) {
    
    /* Tab ID, preview content and Container */
    this.tab = options.tab;
	this.container = options.container;
    this.content = options.content;
    
    /* initialize component generator */
    this.generator = new madCreator({
        id : 26
    });

    /* define the data structure if its empty */
    this.data = Object.keys(options.data).length != 0 ? options.data : {
        'leadGenEle' : [
            {
                inputType : 'combo', // type of input (puretext, text, email, numeric, combo, textarea, radio, checkbox, date, link)
                inputLabel : 'Name', // the label text for the input element. Also the text for hyperlink
                inputValue : '2,3,4,5', // value for placeholder, hyperlink url, list of values for combobox, checkboxes and radio buttons separated by new line
                inputRequired : '1' // determine whether this input is a required field (1=required, 0=not required)
            }
        ],
        'submitButtonText' : 'Submit',
        'successMessage' : 'Thank you!',
        'emailSend' : {
            'enable' : '0', // whether to send the submitted detail to specified email addresses (1=yes, 0=no)
            'emails' : 'i.e abcd@gmail.com' // list of email addresses, separated by comma
        },
        'themeStyle' : {
            'bgColor' : '#ffffff',  // background color of the whole component. Success message overlay background color will follow this as well
            'labelColor' : '#000000', // label's text color
            'textColor' : '#000000', // input text color
            'inputBgColor' : '#ffffff', // background color of input box. Applies to the overlay calendar and comboBox as well
            'placeholderColor' : '#ffffff', // text color for placeholder
            'buttonColor' : '#ffffff', // submit button color
            'buttonTextColor' : '#000000' // submit button text color. Button border color will follow this color
        }
    };
    
    /* @NOTE set structure to pref */
    this.generator.setPrefStructure(this.data);

    var _this = this;

    this.generator.loadCss( 'testComponents/css/style.css' );
    this.generator.loadJs( 'testComponents/js/datepicker.js' );
    this.generator.loadJs( 'testComponents/js/scrollbar.js' );
    this.generator.loadJs( 'testComponents/js/script.js',function() {
        _this.content.html('');
        new LeadGen( {
            id: _this.tab,
            content: _this.content[0],
            width: _this.content.width(),
            height: _this.content.height(),
            componentData: { // data for this component
                leadGenID : '', // id of the lead gen form to retrieve all its input elements
                leadGenTrackID : '', // revision id of the lead gen form
                leadGenEle : _this.data.leadGenEle,
                submissionOption : {
                    submitButtonText : _this.data.submitButtonText, // the text to display on the submit button
                    dataExport : '1', // whether to submit the data back to our own server (1=yes, 0=no)
                    emailSend : _this.data.emailSend,
                    directSubmission : {
                        enable : '0', // whether to send the submitted detail to specified third party platform (1=yes, 0=no)
                        platforms : [] // details of third party platform such as api details, account details, etc. This is for Phase 2
                    }
                },
                themeStyle : _this.data.themeStyle,
                successMessage : 'Thank you!\nWe will keep in touch soon', // the message that is shown after successful submission
                submissionParam : {
                    userId : '140',
                    studioId : '1848',
                    tabId : '1',
                    trackId : '1044',
                    referredURL : 'http://google.com'
                }
            }
        });
    } );
    
    this.render();
}

sampleComponent.prototype.render = function () {
    var _this = this;

    //--------------------------------
    // INPUT LIST SECTION
    //--------------------------------
    var input_list_section_div = document.createElement( 'div' );
    var add_new_input_link = document.createElement( 'a' );

    var input_list_section = this.generator.section( {
        'title' : {
            'title' : 'Input List'
        },
        'expandable' : 'true',
        'content' : 'show',
        'input' : []
    } );

    /* Generate dropdown in popup*/
    var add_input_panel_input_type = this.generator.getSelect( {
        'title' : {
            'title' : 'Input Type'
        },
        'input' : {
            'value' : '',
            'options'   : [
                {text : 'Single Line Text', value : 'text'},
                {text : 'Multiple Line Text', value : 'textarea'},
                {text : 'Email', value : 'email'},
                {text : 'Number', value : 'numeric'},
                {text : 'Dropdown', value : 'combo'},
                {text : 'Single Choice', value : 'radio'},
                {text : 'Multiple Choice', value : 'checkbox'},
                {text : 'Date', value : 'date'},
                {text : 'Link', value : 'link'}
            ]
        },
        'event' : {
            'type' : 'change',
            'callback' : function() {
                switch( this.value ) {
                    case 'combo' :
                    case 'radio' :
                    case 'checkbox' : {
                        add_input_panel_input_choice_value.style.display = 'block';
                        add_input_panel_input_placeholder.style.display = 'none';
                    }
                    break;

                    case 'link' : {
                        add_input_panel_input_label.querySelector( 'span' ).innerHTML = 'Link Label';
                        add_input_panel_input_placeholder.querySelector( 'span' ).innerHTML = 'Link URL';
                        add_input_panel_input_choice_value.style.display = 'none';
                        add_input_panel_input_placeholder.style.display = 'block';
                    }
                    break;

                    default : {
                        add_input_panel_input_label.querySelector( 'span' ).innerHTML = 'Input Label';
                        add_input_panel_input_placeholder.querySelector( 'span' ).innerHTML = 'Input Placeholder';
                        add_input_panel_input_choice_value.style.display = 'none';
                        add_input_panel_input_placeholder.style.display = 'block';
                    }
                    break;
                }
            }
        }
    } );

    /* Generate input label for popup */
    var add_input_panel_input_label = this.generator.getInput( {
        'title' : {
            'title' : 'Input Label'
        },
        'input' : {
            'type' : 'text',
            'value' : ''
        }
    } );

    /* Generate input placeholder for popup */
    var add_input_panel_input_placeholder = this.generator.getInput( {
        'title' : {
            'title' : 'Input Placeholder'
        },
        'input' : {
            'type' : 'text',
            'value' : ''
        }
    } );

    /* Generate input for special input type */
    var add_input_panel_input_choice_value = this.generator.getInput( {
        'title' : {
            'title' : 'Add choice +'
        },
        'input' : {
            'type' : 'text',
            'value' : '',
            'class' : 'input-choice-value'
        }
    } );

    /* Generate checkboxes for popup */
    var add_input_panel_checkboxes = this.generator.getCheckboxes( {
        'input' : {
            'value' : '',
            'checkboxes' : [
                {'text' : 'Make this a require field', 'value' : 'check 1'}
            ]
        }
    } );

    /* Generate button for popup */
    var add_input_panel_add_new_button = this.generator.getButton( {
        'title' : {
            'title' : ''
        },
        'input' : {
            'value' : 'Add'
        },
        'event' : {
            'type' : 'click',
            'callback' : function () {
                var input_value = add_input_panel_input_placeholder.querySelector( 'input' ).value;

                switch( add_input_panel_input_type.querySelector( 'select' ).value ) {
                    case 'combo' :
                    case 'radio' :
                    case 'checkbox' : {
                        input_value = '';

                        $.each( $( add_input_panel ).find( '.input-choice-value' ), function( index, item ) {
                            input_value = input_value + item.value + ',';
                        } );

                        input_value = input_value.substr( 0, input_value.length - 1 );
                    }
                    break;
                }

                _this.data.leadGenEle.push( {
                    'inputType' : add_input_panel_input_type.querySelector( 'select' ).value,
                    'inputLabel' : add_input_panel_input_label.querySelector( 'input' ).value,
                    'inputValue' : input_value,
                    'inputRequired' : add_input_panel_checkboxes.querySelector( 'input' ).checked ? 1 : 0
                } );

                var item = _this.generator.getButton( {
                    'title' : {
                        'title' : ''
                    },
                    'input' : {
                        'value' : add_input_panel_input_label.querySelector( 'input' ).value + '(' + add_input_panel_input_type.querySelector( 'select' ).value + ')',
                        'data-json': JSON.stringify( {
                            'inputType' : add_input_panel_input_type.querySelector( 'select' ).value,
                            'inputLabel' : add_input_panel_input_label.querySelector( 'input' ).value,
                            'inputValue' : input_value,
                            'inputRequired' : add_input_panel_checkboxes.querySelector( 'input' ).checked ? 1 : 0
                        } )
                    }
                } );

                item.querySelector( '.ad_creator_sub_btn' ).addEventListener( 'click', edit_input );

                add_input_panel_input_label.querySelector( 'input' ).value = '';
                add_input_panel_input_placeholder.querySelector( 'input' ).value = '';
                $.each( $( add_input_panel_input_choice_value ).find( 'div' ), function( index, item ) {
                    item.remove();
                } );
                add_input_panel_input_choice_value.querySelector( 'input' ).value = '';
                add_input_panel_checkboxes.querySelector( 'input' ).checked = false;

                item.querySelector( 'div' ).style.width = '100%';
                input_list_section_div.append( item );
                $( input_list_section_div ).sortable( {
                    beforeStop: function() {
                        re_generate_input_data();
                        _this.render_content();
                    }
                } );

                add_input_panel.style.display = 'none';
                _this.render_content();
            }
        }
    } );   

    /* Generate add input popup */
    var add_input_panel = this.generator.getPanel( {
        'title' : 'Add new input',
        'class' : 'add-input-panel',
        'input' : [
            add_input_panel_input_type, 
            add_input_panel_input_label, 
            add_input_panel_input_choice_value, 
            add_input_panel_input_placeholder, 
            add_input_panel_checkboxes,
            add_input_panel_add_new_button
        ]
    } );

    input_list_section_div.setAttribute( 'class', 'input-list' );
    input_list_section_div.setAttribute( 'id', 'sortable' );
    add_new_input_link.setAttribute( 'href', '#lgcpn' );
    add_new_input_link.setAttribute( 'style', 'display: block; text-align: center; margin-top: 20px;' );
    add_new_input_link.innerHTML = 'Add new input';
    input_list_section.querySelector( '.section' ).append( input_list_section_div );
    input_list_section.querySelector( '.section' ).append( add_new_input_link );
    input_list_section.querySelector( '.section' ).style.display = 'block';
    add_input_panel_input_choice_value.style.display = 'none';

    add_new_input_link.addEventListener( 'click', function() {
        add_input_panel.style.display = 'block';
    } );

    add_input_panel_input_choice_value.querySelector( 'span' ).addEventListener( 'click', add_input_field );

    /* Generate default value for input list section*/
    for( var i = 0; i < _this.data.leadGenEle.length; i++ ) {
        input_list_section_div.append( 
            _this.generator.getButton( {
                'title' : {
                    'title' : ''
                },
                'input' : {
                    'value' : _this.data.leadGenEle[i].inputLabel + '(' + _this.data.leadGenEle[i].inputType + ')',
                    'style' : 'width: 100%;',
                    'data-json': JSON.stringify( {
                        'inputType' : _this.data.leadGenEle[i].inputType,
                        'inputLabel' : _this.data.leadGenEle[i].inputLabel,
                        'inputValue' : _this.data.leadGenEle[i].inputValue,
                        'inputRequired' : _this.data.leadGenEle[i].inputRequired
                    } )
                }
            } ) 
        );
    }

    $( input_list_section_div ).sortable( {
        beforeStop: function() {
            var leadGenEle = [];

            $.each( $( input_list_section_div ).find( '.ad_creator_sub_btn' ), function( index, item ) {
                leadGenEle.push( JSON.parse( $( item ).attr( 'data-json' ) ) );
            } );

            _this.data.leadGenEle = leadGenEle;
            _this.render_content();
        }
    } );

    /* Edit input panel */
    /* Generate edit input popup */
    var edit_input_panel = this.generator.getPanel( {
        'title' : 'Edit input',
        'class' : 'edit-input-panel',
        'input' : []
    } );

    /* Generate dropdown in popup*/
    var edit_input_panel_input_type = this.generator.getSelect( {
        'title' : {
            'title' : 'Input Type'
        },
        'input' : {
            'value' : '',
            'options'   : [
                {text : 'Single Line Text', value : 'text'},
                {text : 'Multiple Line Text', value : 'textarea'},
                {text : 'Email', value : 'email'},
                {text : 'Number', value : 'numeric'},
                {text : 'Dropdown', value : 'combo'},
                {text : 'Single Choice', value : 'radio'},
                {text : 'Multiple Choice', value : 'checkbox'},
                {text : 'Date', value : 'date'},
                {text : 'Link', value : 'link'}
            ]
        },
        'event' : {
            'type' : 'change',
            'callback' : function() {
                switch( this.value ) {
                    case 'combo' :
                    case 'radio' :
                    case 'checkbox' : {
                        edit_input_panel_input_choice_value.style.display = 'block';
                        edit_input_panel_input_placeholder.style.display = 'none';
                    }
                    break;

                    case 'link' : {
                        edit_input_panel_input_label.querySelector( 'span' ).innerHTML = 'Link Label';
                        edit_input_panel_input_placeholder.querySelector( 'span' ).innerHTML = 'Link URL';
                        edit_input_panel_input_choice_value.style.display = 'none';
                        edit_input_panel_input_placeholder.style.display = 'block';
                    }
                    break;

                    default : {
                        edit_input_panel_input_label.querySelector( 'span' ).innerHTML = 'Input Label';
                        edit_input_panel_input_placeholder.querySelector( 'span' ).innerHTML = 'Input Placeholder';
                        edit_input_panel_input_choice_value.style.display = 'none';
                        edit_input_panel_input_placeholder.style.display = 'block';
                    }
                    break;
                }
            }
        }
    } );

    /* Generate input label for popup */
    var edit_input_panel_input_label = this.generator.getInput( {
        'title' : {
            'title' : 'Input Label'
        },
        'input' : {
            'type' : 'text',
            'value' : ''
        }
    } );

    /* Generate input placeholder for popup */
    var edit_input_panel_input_placeholder = this.generator.getInput( {
        'title' : {
            'title' : 'Input Placeholder'
        },
        'input' : {
            'type' : 'text',
            'value' : ''
        }
    } );

    /* Generate input for special input type */
    var edit_input_panel_input_choice_value = this.generator.getInput( {
        'title' : {
            'title' : 'Add choice +'
        },
        'input' : {
            'type' : 'text',
            'value' : '',
            'class' : 'input-choice-value'
        }
    } );

    /* Generate checkboxes for popup */
    var edit_input_panel_checkboxes = this.generator.getCheckboxes( {
        'input' : {
            'value' : '',
            'checkboxes' : [
                {'text' : 'Make this a require field', 'value' : 'check 1'}
            ]
        }
    } );

    /* Generate button for popup */
    var edit_input_panel_edit_button = this.generator.getButton( {
        'title' : {
            'title' : ''
        },
        'input' : {
            'value' : 'edit'
        },
        'event' : {
            'type' : 'click',
            'callback' : function () {
                var input_value = edit_input_panel_input_placeholder.querySelector( 'input' ).value;

                switch( edit_input_panel_input_type.querySelector( 'select' ).value ) {
                    case 'combo' :
                    case 'radio' :
                    case 'checkbox' : {
                        input_value = '';
                        $.each( $( edit_input_panel ).find( '.input-choice-value' ), function( index, item ) {
                            input_value = input_value + item.value + ',';
                        } );

                        input_value = input_value.substr( 0, input_value.length - 1 );
                    }
                    break;
                }

                edit_element.setAttribute( 'data-json', JSON.stringify( 
                    {
                        'inputType' : edit_input_panel_input_type.querySelector( 'select' ).value,
                        'inputLabel' : edit_input_panel_input_label.querySelector( 'input' ).value,
                        'inputValue' : input_value,
                        'inputRequired' : edit_input_panel_checkboxes.querySelector( 'input' ).checked ? 1 : 0
                    } 
                ) );

                edit_element.innerHTML = edit_input_panel_input_label.querySelector( 'input' ).value + '(' + edit_input_panel_input_type.querySelector( 'select' ).value + ')';

                edit_input_panel_input_label.querySelector( 'input' ).value = '';
                add_input_panel_input_placeholder.querySelector( 'input' ).value = '';

                $.each( $( edit_input_panel_input_choice_value ).find( 'div' ), function( index, item ) {
                    item.remove();
                } );

                edit_input_panel_input_choice_value.querySelector( 'input' ).value = '';
                edit_input_panel_checkboxes.querySelector( 'input' ).checked = false;

                re_generate_input_data();
                edit_input_panel.style.display = 'none';
                _this.render_content();
            }
        }
    } );   

    edit_input_panel_edit_button.style.display = 'inline-block';

    /* Generate button for popup */
    var edit_input_panel_delete_button = this.generator.getButton( {
        'title' : {
            'title' : ''
        },
        'input' : {
            'value' : 'delete'
        },
        'event' : {
            'type' : 'click',
            'callback' : function () {
                edit_element.parentNode.removeChild( edit_element );
                re_generate_input_data();
                edit_input_panel.style.display = 'none';
                _this.render_content();
            }
        }
    } );

    edit_input_panel_delete_button.style.display = 'inline-block';

    /* Generate edit input popup */
    var edit_input_panel = this.generator.getPanel( {
        'title' : 'Edit input',
        'class' : 'edit-input-panel',
        'input' : [
            edit_input_panel_input_type, 
            edit_input_panel_input_label, 
            edit_input_panel_input_choice_value, 
            edit_input_panel_input_placeholder, 
            edit_input_panel_checkboxes,
            edit_input_panel_edit_button,
            edit_input_panel_delete_button
        ]
    } );

    edit_input_panel_input_choice_value.style.display = 'none';

    $( input_list_section_div ).find( '.ad_creator_sub_btn' ).click( edit_input );

    edit_input_panel_input_choice_value.querySelector( 'span' ).addEventListener( 'click', add_input_field );

    /* Share function */
    function edit_input() {
        edit_input_panel.style.display = 'block';
        edit_element = this;
        var data = JSON.parse( $( this ).attr( 'data-json' ) );
        edit_input_panel_input_type.querySelector( 'select' ).value = data.inputType;
        edit_input_panel_input_label.querySelector( 'input' ).value = data.inputLabel;
        edit_input_panel_input_placeholder.querySelector( 'input' ).value = data.inputValue;
        edit_input_panel_checkboxes.querySelector( 'input' ).checked = data.inputRequired == 0 ? false : true
        console.log( data.inputType );
        switch( data.inputType ) {
            case 'combo' :
            case 'radio' :
            case 'checkbox' : {
                edit_input_panel_input_choice_value.style.display = 'block';
                edit_input_panel_input_placeholder.style.display = 'none';

                var input_data = data.inputValue.split( ',' );
                
                for( var i = 0; i < input_data.length; i++ ) {
                    if( i == 0 ) {
                       edit_input_panel_input_choice_value.querySelector( 'input' ).value = input_data[i];
                    } else {
                        var additional_choice = edit_input_panel_input_choice_value.querySelector( 'input' ).cloneNode( true );
                        var minus_span = document.createElement( 'span' );
                        var choice_wrapper = document.createElement( 'div' );

                        choice_wrapper.append( additional_choice );
                        choice_wrapper.append( minus_span );
                        minus_span.innerHTML = '  -';
                        edit_input_panel_input_choice_value.append( choice_wrapper );
                        additional_choice.value = input_data[i];

                        minus_span.addEventListener( 'click', function() {
                            var parent_node = this.parentNode;
                            parent_node.parentNode.removeChild( parent_node );
                        } );
                    }
                }
            }
            break;

            case 'link' : {
                edit_input_panel_input_label.querySelector( 'span' ).innerHTML = 'Link Label';
                edit_input_panel_input_placeholder.querySelector( 'span' ).innerHTML = 'Link URL';
                edit_input_panel_input_choice_value.style.display = 'none';
                edit_input_panel_input_placeholder.style.display = 'block';
            }
            break;

            default : {
                edit_input_panel_input_label.querySelector( 'span' ).innerHTML = 'Input Label';
                edit_input_panel_input_placeholder.querySelector( 'span' ).innerHTML = 'Input Placeholder';
                edit_input_panel_input_choice_value.style.display = 'none';
                edit_input_panel_input_placeholder.style.display = 'block';
            }
            break;
        }
    } 

    function re_generate_input_data() {
        var leadGenEle = [];

        $.each( $( input_list_section_div ).find( '.ad_creator_sub_btn' ), function( index, item ) {
            leadGenEle.push( JSON.parse( $( item ).attr( 'data-json' ) ) );
        } );

        _this.data.leadGenEle = leadGenEle;
    }

    function add_input_field() {
        console.log( this );
        var additional_choice = this.nextSibling.cloneNode( true )
        var minus_span = document.createElement( 'span' );
        var choice_wrapper = document.createElement( 'div' );

        choice_wrapper.append( additional_choice );
        choice_wrapper.append( minus_span );
        minus_span.innerHTML = '  -';
        edit_input_panel_input_choice_value.append( choice_wrapper );
        additional_choice.value = '';

        minus_span.addEventListener( 'click', function() {
            var parent_node = this.parentNode;
            parent_node.parentNode.removeChild( parent_node );
        } );
    } 

    //-------------------------------------
    // END OF INPUT LIST SECTION
    //-------------------------------------

    //-------------------------------------
    // SUBMISSION SECTION
    //-------------------------------------
    /* Generate input fo button text inside submission */
    var submission_section_button_text = this.generator.getInput( {
        'title' : {
            'title' : 'Submit button text'
        },
        'input' : {
            'type' : 'text',
            'value' : _this.data.submitButtonText
        },
        'event' : {
            'type' : 'keyup',
            'callback' : function () {
                _this.data.submitButtonText = this.value;
                _this.render_content();
            }
        }
    } );

    /* Generate input for submission message */
    var submission_section_submission_message = this.generator.getTextArea( {
        'title' : {
            'title' : 'Submission Message'
        },
        'input' : {
            'value' : _this.data.successMessage
        },
        'event' : {
            'type' : 'keyup',
            'callback' : function () {
                _this.data.successMessage = this.value;
                _this.render_content();
            }
        }
    } );

    /* Generate email field for submission */
    var submission_section_email = this.generator.getTextArea( {
        'title' : {
            'title' : ''
        },
        'input' : {
            'value' : _this.data.emailSend.emails
        },
        'event' : {
            'type' : 'keyup',
            'callback' : function () {
                _this.data.emailSend.emails = submission_section_email.querySelector( 'textarea' ).value;
            }
        },
    } );

    /* Generate checkbox for submission email nortification  */
    var submission_section_submission_nortification = this.generator.getCheckboxes( {
        'input' : {
            'value' : '',
            'checkboxes' : [
                {'text' : 'Instant Email Nortification', 'value' : 'true'}
            ]
        },
        'event' : {
            'type' : 'click',
            'callback' : function () {
                _this.data.emailSend.enable = this.checked ? 1 : 0;

                if( this.checked ) {
                    _this.data.emailSend.emails = submission_section_email.querySelector( 'textarea' ).value;
                }
            }
        }
    } );

    var submission_section = this.generator.section( {
        'title' : {
            'title' : 'Submission Options'
        },
        'expandable' : 'true',
        'content' : 'show',
        'input' : [
            submission_section_button_text, 
            submission_section_submission_message, 
            submission_section_submission_nortification, 
            submission_section_email
        ]
    } );

    submission_section.querySelector( '.section' ).style.display = 'block';
    submission_section_submission_nortification.querySelector( 'input' ).checked = _this.data.emailSend.enable == 1 ? true : false;

    if( !submission_section_submission_nortification.querySelector( 'input' ).checked ) {
        submission_section_email.style.display = 'none';   
    }

    submission_section_submission_nortification.addEventListener( 'click', function() {
        if( submission_section_submission_nortification.querySelector( 'input' ).checked ) {
            submission_section_email.style.display = 'block';
        } else {
            submission_section_email.style.display = 'none';
        }
    } );
    //-------------------------------------
    // END OF SUBMISSION SECTION
    //-------------------------------------

    //-------------------------------------
    // START LOOK AND FIELD
    //-------------------------------------
    var look_and_field_section_background_color = this.generator.getColorPicker( {
        'title' : {
            'title' : 'Background Color'
        },
        'input': {
            'value' : _this.data.themeStyle.bgColor
        },
        'event' : {
            'type' : 'focusout',
            'callback' : function () {
                _this.data.themeStyle.bgColor = this.value;
                _this.render_content();
            }
        }
    } );

    var look_and_field_section_input_label_color = this.generator.getColorPicker( {
        'title' : {
            'title' : 'Input Label Color'
        },
        'input': {
            'value' : _this.data.themeStyle.labelColor
        },
        'event' : {
            'type' : 'focusout',
            'callback' : function () {
                _this.data.themeStyle.labelColor = this.value;
                _this.render_content();
            }
        }
    } );

    var look_and_field_section_input_text_color = this.generator.getColorPicker( {
        'title' : {
            'title' : 'Input Text Color'
        },
        'input': {
            'value' : _this.data.themeStyle.textColor
        },
        'event' : {
            'type' : 'focusout',
            'callback' : function () {
                _this.data.themeStyle.textColor = this.value;
                _this.render_content();
            }
        }
    } );

    var look_and_field_section_input_background_color = this.generator.getColorPicker( {
        'title' : {
            'title' : 'Input Background Color'
        },
        'input': {
            'value' : _this.data.themeStyle.inputBgColor
        },
        'event' : {
            'type' : 'focusout',
            'callback' : function () {
                _this.data.themeStyle.inputBgColor = this.value;
                _this.render_content();
            }
        }
    } );

    var look_and_field_section_placeholder_text_color = this.generator.getColorPicker( {
        'title' : {
            'title' : 'Placeholder Text Color'
        },
        'input': {
            'value' : _this.data.themeStyle.placeholderColor
        },
        'event' : {
            'type' : 'focusout',
            'callback' : function () {
                _this.data.themeStyle.placeholderColor = this.value;
                _this.render_content();
            }
        }
    } );

    var look_and_field_section_button_background_color = this.generator.getColorPicker( {
        'title' : {
            'title' : 'Button Background Color'
        },
        'input': {
            'value' : _this.data.themeStyle.buttonColor
        },
        'event' : {
            'type' : 'focusout',
            'callback' : function () {
                _this.data.themeStyle.buttonColor = this.value;
                _this.render_content();
            }
        }
    } );

    var look_and_field_section_button_text_color = this.generator.getColorPicker( {
        'title' : {
            'title' : 'Button Text Color'
        },
        'input': {
            'value' : _this.data.themeStyle.buttonTextColor
        },
        'event' : {
            'type' : 'focusout',
            'callback' : function () {
                _this.data.themeStyle.buttonTextColor = this.value;
                _this.render_content();
            }
        }
    } );    

    var look_and_field_section = this.generator.section( {
        'title' : {
            'title' : 'Look & Field'
        },
        'expandable' : 'true',
        'content' : 'show',
        'input' : [
            look_and_field_section_background_color, 
            look_and_field_section_input_label_color, 
            look_and_field_section_input_text_color, 
            look_and_field_section_input_background_color, 
            look_and_field_section_placeholder_text_color, 
            look_and_field_section_button_background_color, 
            look_and_field_section_button_text_color
        ]
    } );        

    look_and_field_section.querySelector( '.section' ).style.display = 'block';
    //-------------------------------------
    // END OF LOOK AND FIELD
    //-------------------------------------

    this.container.append( input_list_section );
    this.container.append( submission_section );
    this.container.append( look_and_field_section );
    
}

sampleComponent.prototype.render_content = function() {
    this.content.html( '' );
    new LeadGen( {
        id: this.tab,
        content: this.content[0],
        width: this.content.width(),
        height: this.content.height(),
        componentData: { // data for this component
            leadGenID : '', // id of the lead gen form to retrieve all its input elements
            leadGenTrackID : '', // revision id of the lead gen form
            leadGenEle : this.data.leadGenEle,
            submissionOption : {
                submitButtonText : this.data.submitButtonText, // the text to display on the submit button
                dataExport : '1', // whether to submit the data back to our own server (1=yes, 0=no)
                emailSend : this.data.emailSend,
                directSubmission : {
                    enable : '0', // whether to send the submitted detail to specified third party platform (1=yes, 0=no)
                    platforms : [] // details of third party platform such as api details, account details, etc. This is for Phase 2
                }
            },
            themeStyle : this.data.themeStyle,
            successMessage : 'Thank you!\nWe will keep in touch soon', // the message that is shown after successful submission
            submissionParam : {
                userId : '140',
                studioId : '1848',
                tabId : '1',
                trackId : '1044',
                referredURL : 'http://google.com'
            }
        }
    });
}