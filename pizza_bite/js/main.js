/*
*
* mads - version 2.00.01
* Copyright (c) 2015, Ninjoe
* Dual licensed under the MIT or GPL Version 2 licenses.
* https://en.wikipedia.org/wiki/MIT_License
* https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
*
*/
var mads = function (options) {

    var _this = this;

    this.render = options.render;

    /* Body Tag */
    this.bodyTag = document.getElementsByTagName('body')[0];

    /* Head Tag */
    this.headTag = document.getElementsByTagName('head')[0];

    /* json */
    if (typeof json == 'undefined' && typeof rma != 'undefined') {
        this.json = rma.customize.json;
    } else if (typeof json != 'undefined') {
        this.json = json;
    } else {
        this.json = '';
    }

    /* fet */
    if (typeof fet == 'undefined' && typeof rma != 'undefined') {
        this.fet = typeof rma.fet == 'string' ? [rma.fet] : rma.fet;
    } else if (typeof fet != 'undefined') {
        this.fet = fet;
    } else {
        this.fet = [];
    }

    this.fetTracked = false;

    /* load json for assets */
    this.loadJs(this.json, function () {
        _this.data = json_data;

        _this.render.render();
    }); 

    /* Get Tracker */
    if (typeof custTracker == 'undefined' && typeof rma != 'undefined') {
        this.custTracker = rma.customize.custTracker;
    } else if (typeof custTracker != 'undefined') {
        this.custTracker = custTracker;
    } else {
        this.custTracker = [];
    }

    /* CT */
    if (typeof ct == 'undefined' && typeof rma != 'undefined') {
        this.ct = rma.ct;
    } else if (typeof ct != 'undefined') {
        this.ct = ct;
    } else {
        this.ct = [];
    }

    /* CTE */
    if (typeof cte == 'undefined' && typeof rma != 'undefined') {
        this.cte = rma.cte;
    } else if (typeof cte != 'undefined') {
        this.cte = cte;
    } else {
        this.cte = [];
    }
    
    /* tags */
    if (typeof tags == 'undefined' && typeof tags != 'undefined') {
        this.tags = this.tagsProcess(rma.tags);
    } else if (typeof tags != 'undefined') {
        this.tags = this.tagsProcess(tags);
    } else {
        this.tags = '';
    }

    /* Unique ID on each initialise */
    this.id = this.uniqId();

    /* Tracked tracker */
    this.tracked = [];
    /* each engagement type should be track for only once and also the first tracker only */
    this.trackedEngagementType = [];
    /* trackers which should not have engagement type */
    this.engagementTypeExlude = [];
    /* first engagement */
    this.firstEngagementTracked = false;

    /* RMA Widget - Content Area */
    this.contentTag = document.getElementById('rma-widget');

    /* URL Path */
    this.path = typeof rma != 'undefined' ? rma.customize.src : '';

    /* Solve {2} issues */
    for (var i = 0; i < this.custTracker.length; i++) {
        if (this.custTracker[i].indexOf('{2}') != -1) {
            this.custTracker[i] = this.custTracker[i].replace('{2}', '{{type}}');
        }
    }
};

/* Generate unique ID */
mads.prototype.uniqId = function () {

    return new Date().getTime();
}

mads.prototype.tagsProcess = function (tags) {
    
    var tagsStr = '';
    
    for(var obj in tags){
        if(tags.hasOwnProperty(obj)){
            tagsStr+= '&'+obj + '=' + tags[obj];
        }
    }     
    
    return tagsStr;
}

/* Link Opner */
mads.prototype.linkOpener = function (url) {

	if(typeof url != "undefined" && url !=""){

        if(typeof this.ct != 'undefined' && this.ct != '') {
            url = this.ct + encodeURIComponent(url);
        }

		if (typeof mraid !== 'undefined') {
			mraid.open(url);
		}else{
			window.open(url);
		}

        if(typeof this.cte != 'undefined' && this.cte != '') {
            this.imageTracker(this.cte);
        }
	}
}

/* tracker */
mads.prototype.tracker = function (tt, type, name, value) {

    /*
    * name is used to make sure that particular tracker is tracked for only once
    * there might have the same type in different location, so it will need the name to differentiate them
    */
    name = name || type;

    if ( tt == 'E' && !this.fetTracked ) {
        for ( var i = 0; i < this.fet.length; i++ ) {
            var t = document.createElement('img');
            t.src = this.fet[i];

            t.style.display = 'none';
            this.bodyTag.appendChild(t);
        }
        this.fetTracked = true;
    }

    if ( typeof this.custTracker != 'undefined' && this.custTracker != '' && this.tracked.indexOf(name) == -1 ) {
        for (var i = 0; i < this.custTracker.length; i++) {
            var img = document.createElement('img');

            if (typeof value == 'undefined') {
                value = '';
            }

            /* Insert Macro */
            var src = this.custTracker[i].replace('{{rmatype}}', type);
            src = src.replace('{{rmavalue}}', value);

            /* Insert TT's macro */
            if (this.trackedEngagementType.indexOf(tt) != '-1' || this.engagementTypeExlude.indexOf(tt) != '-1') {
                src = src.replace('tt={{rmatt}}', '');
            } else {
                src = src.replace('{{rmatt}}', tt);
                this.trackedEngagementType.push(tt);
            }

            /* Append ty for first tracker only */
            if (!this.firstEngagementTracked && tt == 'E') {
                src = src + '&ty=E';
                this.firstEngagementTracked = true;
            }

            /* */
            img.src = src + this.tags + '&' + this.id;

            img.style.display = 'none';
            this.bodyTag.appendChild(img);

            this.tracked.push(name);
        }
    }
};

mads.prototype.imageTracker = function (url) {
    for ( var i = 0; i < url.length; i++ ) {
        var t = document.createElement('img');
        t.src = url[i];

        t.style.display = 'none';
        this.bodyTag.appendChild(t);
    }
}

/* Load JS File */
mads.prototype.loadJs = function (js, callback) {
    var script = document.createElement('script');
    script.src = js;

    if (typeof callback != 'undefined') {
        script.onload = callback;
    }

    this.headTag.appendChild(script);
}

/* Load CSS File */
mads.prototype.loadCss = function (href) {
    var link = document.createElement('link');
    link.href = href;
    link.setAttribute('type', 'text/css');
    link.setAttribute('rel', 'stylesheet');

    this.headTag.appendChild(link);
}

/*
*
* Unit Testing for mads
*
*/
var pizza = function () {
    var _this = this;

    _this.image_path = 'images/';
    _this.css_path = '';
    _this.js_path = 'js/';

    /* pass in object for render callback */
    this.app = new mads({
        'render' : this
    });

    this.app.loadJs('https://code.jquery.com/jquery-1.11.3.min.js',function () {
        _this.app.loadJs( _this.js_path + 'jquery-ui.min.js',function () {
            _this.render();
            ( function() {
                var posX;
                var posY;
                var is_pull_hand_disable = false;

                $( _this.drag_container ).draggable( {
                    handle: $( _this.pull_hand ),
                    helper: function() {
                        return document.createElement( 'dont-move' );
                    }
                } );

                _this.drag_container.addEventListener( 'dragstart', function( event ) {
                    posX = event.clientX;
                    posY = -( event.clientY );
                } );

                $( _this.drag_container ).on( 'drag', function( event ) {
                    if( event.clientX != 0 || event.clientX != 0 ) {
                        var left = event.clientX - posX;
                        var bottom = -( event.clientY - posY );
                        var current_left = isNaN( parseInt( _this.drag_container.style.left.replace( /px/i, '' ) ) ) ?
                        0 : 
                        parseInt( _this.drag_container.style.left.replace( /px/i, '' ) );
                        var current_bottom = isNaN( parseInt( _this.drag_container.style.bottom.replace( /px/i, '' ) ) ) ?
                        0 : 
                        parseInt( _this.drag_container.style.bottom.replace( /px/i, '' ) );
                        posX = event.clientX;
                        posY = event.clientY;
                        if( !is_pull_hand_disable ) {
                            // Moving the hand and the cheese
                            _this.drag_container.style.left = current_left + left + 'px';
                            _this.drag_container.style.bottom = current_bottom + bottom + 'px';
                        }

                        //Show and hide other element
                        _this.show( _this.pull_hand );
                        _this.hand_icon.setAttribute( 'class', 'hand-icon' );
                        _this.hide( _this.hand_icon );
                        _this.hide( _this.drag_text_1 );
                        _this.show( _this.drag_text_2 );

                        //If hand reach the required height
                        if( current_bottom + bottom > 44 ) {
                            _this.show( _this.hand_pull_cheese_text );
                            _this.hand_pull_cheese_text.style.left = parseInt( _this.drag_container.style.left.replace( /px/i, '' ) ) + 126 + 'px';
                            is_pull_hand_disable = true;

                            // Show hand above
                            _this.hand_above.style.top = 0;
                            setTimeout( function() {
                                _this.show( _this.hand_above_text );
                                _this.hand_icon.setAttribute( 'class', 'hand-icon hand-icon-animation-top' );
                                _this.show( _this.hand_icon );
                                $( _this.hand_above ).draggable( {
                                    cursorAt: { left: 10, top: 20 },
                                    drag: function( event, ui ) {
                                        _this.hand_icon.setAttribute( 'class', '' );
                                        _this.hide( _this.drag_text_2 );
                                    },
                                    helper: function() {
                                        return _this.create_element( {
                                            element_name: 'img',
                                            element_class: 'pull-hand',
                                            element_data: {
                                                style: {
                                                    left: '20px',
                                                    zIndex: '7'
                                                },
                                                src: _this.image_path + 'above-to-sauce.png'
                                            }
                                        } );
                                    }
                                } );

                                $( _this.hand_above_text ).draggable( {
                                    cursorAt: { left: 10, top: 20 },
                                    drag: function( event, ui ) {
                                        _this.hand_icon.setAttribute( 'class', '' );
                                        _this.hide( _this.drag_text_2 );
                                    },
                                    helper: function() {
                                        return _this.create_element( {
                                            element_name: 'img',
                                            element_class: 'pull-hand',
                                            element_data: {
                                                style: {
                                                    left: '20px',
                                                    zIndex: '7'
                                                },
                                                src: _this.image_path + 'above-to-sauce.png'
                                            }
                                        } );
                                    }
                                } );
                            }, 1000 );

                            //Add droppable to the pizza yellow sauce at the bottom
                            $(  _this.sauces_zone ).droppable( {
                                drop: function( event, ui ) {
                                    //Show hand right and text
                                    _this.hand_right.style.right = 0;
                                    setTimeout( function() {
                                        _this.show( _this.hand_right_text );
                                    }, 500 );
                                }
                            } );
                        }
                    }
                } );               
            } )();
        });
    });

    this.app.loadCss('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css');
    this.app.loadCss(_this.css_path + 'css/customize.css');
}

/* 
* render function 
* - render has to be done in render function 
* - render will be called once json data loaded
*/
pizza.prototype.render = function () { 
    var _this = this;

    _this.pull_hand = _this.create_element( {
        element_name: 'img',
        element_class: 'pull-hand',
        element_data: {
            style: {
                bottom: '137px',
                left: '0px',
                position: 'absolute',
                opacity: '0',
                zIndex: '4'
            },
            src: _this.image_path + 'hand-pull-cheese.png'
        }
    } );

    _this.pizza_pieces = _this.create_element( {
        element_name: 'img',
        element_class: 'pizza-piecies',
        element_data: {
            style: {
                bottom: '133px',
                left: '91px',
                position: 'absolute',
                zIndex: '4'
            },
            src: _this.image_path + 'pizza-pieces.png'
        }
    } );

    _this.drag_container = _this.create_element( {
        element_name: 'div',
        element_class: 'drag-container',
        element_data: {
            style: {
                cursor: 'pointer',
                position: 'relative',
                width: '100%',
                height: '100%'
            },
            child: [_this.pizza_pieces, _this.pull_hand]
        }
    } );

    _this.hand_icon = _this.create_element( {
        element_name: 'img',
        element_class: 'hand-icon hand-icon-animation',
        element_data: {
            style: {
                position: 'absolute',
                zIndex: '8'
            },
            src: _this.image_path + 'drag-icn.png'
        }
    } );

    _this.info_btn = _this.create_element( {
        element_name: 'img',
        element_class: 'info-btn',
        element_data: {
            style: {
                bottom: '0px',
                cursor: 'pointer',
                left: '90px',
                position: 'absolute',
                zIndex: '4'
            },
            event: {
                click: function() {
                    _this.app.linkOpener('https://urldefense.proofpoint.com/v2/url?u=https-3A__www.pizzahut.co.id_promosi_menu-2Dbaru&d=DwMFAg&c=N-xPqDyeLJg5V3gLll2thA&r=FwcQ0jWqIbswdMAX32-yBfPYP1XI3qYgnXL3Df1feBU&m=hOj2UhwygP7Zar8CKD2c-TuURTHjLiZa3O-W2KXthgs&s=3S4pAm3WBLBzkbRJCSPQeYrnnA-kjXy5-UbJhoL19_c&e=');
                }
            },
            src: _this.image_path + 'info-btn.png'
        }
    } );

    _this.drag_text_1 = _this.create_element( {
        element_name: 'img',
        element_class: 'drag-text-1',
        element_data: {
            style: {
                bottom: '210px',
                left: '100px',
                position: 'absolute'
            },
            src: _this.image_path + 'drag-text-1.png'
        }
    } );

    _this.drag_text_2 = _this.create_element( {
        element_name: 'img',
        element_class: 'drag-text-2',
        element_data: {
            style: {
                bottom: '210px',
                left: '170px',
                opacity: '0',
                position: 'absolute'
            },
            src: _this.image_path + 'drag-text-2.png'
        }
    } );

    _this.hand_pull_cheese_text = _this.create_element( {
        element_name: 'img',
        element_class: 'hand-pull-cheese-text',
        element_data: {
            style: {
                bottom: '193px',
                left: '90px',
                opacity: '0',
                position: 'absolute'
            },
            src: _this.image_path + 'hand-pull-cheese-text.png'
        }
    } );

    _this.hand_above = _this.create_element( {
        element_name: 'img',
        element_class: 'hand-above-text',
        element_data: {
            style: {
                top: '-121px',
                left: '0px',
                position: 'absolute',
                transition: 'top 1s',
                zIndex: '1'
            },
            src: _this.image_path + 'hand-above.png'
        }
    } );

    _this.hand_above_text = _this.create_element( {
        element_name: 'img',
        element_class: 'hand-above-text',
        element_data: {
            style: {
                top: '55px',
                left: '10px',
                opacity: '0',
                position: 'absolute',
                zIndex: '4'
            },
            src: _this.image_path + 'hand-above-text.png'
        }
    } );

    _this.sauces_zone = _this.create_element( {
        element_name: 'div',
        element_class: 'sauces_zone',
        element_data: {
            style: {
                bottom: '60px',
                height: '70px',
                left: '159px',
                opacity: '1',
                position: 'absolute',
                width: '70px',
                zIndex: '10'
            }
        }
    } );

    _this.hand_right = _this.create_element( {
        element_name: 'img',
        element_class: 'hand-right',
        element_data: {
            style: {
                bottom: '180px',
                position: 'absolute',
                right: '-53px',
                transition: 'right 0.5s'
            },
            src: _this.image_path + 'hand-right.png'
        }
    } );

    _this.hand_right_text = _this.create_element( {
        element_name: 'img',
        element_class: 'hand-right-text',
        element_data: {
            style: {
                bottom: '210px',
                opacity: '0',
                position: 'absolute',
                right: '36px'
            },
            src: _this.image_path + 'hand-right-text.png'
        }
    } );

    _this.container = _this.create_element( {
        element_name: 'div',
        element_class: 'container',
        element_data: {
            style: {
                background: 'url("' + _this.image_path + 'bg-without-pizza.png' + '")',
                height: '480px',
                maxWidth: '320px',
                overflow: 'hidden',
                paddingLeft: '0',
                paddingRight: '0',
                position: 'relative'
            },
            child: [
                _this.drag_container, 
                _this.hand_icon, 
                _this.info_btn, 
                _this.drag_text_1, 
                _this.drag_text_2,
                _this.hand_pull_cheese_text, 
                _this.hand_above,
                _this.hand_above_text,
                _this.sauces_zone,
                _this.hand_right,
                _this.hand_right_text
            ]
        }
    } );

    _this.app.contentTag.appendChild( _this.container );

}

/* 
* create_element function
* use like document.createElement but add some parameter
*/
pizza.prototype.create_element = function(element_data) {
    // If not have tag name exit
    if (!element_data.element_name) {
        return;
    }

    var element_name = element_data.element_name;
    var element_id = element_data.element_id;
    var element_class = element_data.element_class;
    var element_data = element_data.element_data ? element_data.element_data : [];
    var element_content = element_data.content ? element_data.content : '';

    // Create element
    var element = document.createElement(element_name);
    element.innerHTML = element_content;

    if (element_id) {
        element.setAttribute('id', element_id);
    }

    if (element_class) {
        element.setAttribute('class', element_class);
    }

    for (var key in element_data) {
        switch (key) {
            case 'event':
                // Adding event for element
                for (var key in element_data.event) {
                    if (typeof(element_data.event[key]) == 'function') {
                        element.addEventListener(key, element_data.event[key]);
                    }
                }

                break;

            case 'style':
                var style = '';

                for (var key in element_data.style) {
                    style = style + key.replace(/[A-Z]/g, function myFunction(chr) {
                        return '-' + chr.toLowerCase();
                    }) + ':' + element_data.style[key] + ';';
                }

                element.setAttribute('style', style);
                break;

            case 'child':
                for (var i = 0; i < element_data.child.length; i++) {
                    if (typeof(element_data.child[i]) == 'object') {
                        element.appendChild(element_data.child[i]);
                    }
                }

                break;

            case 'content':
                //igrone content
                break;

            default:
                element.setAttribute(key, element_data[key]);
                break;
        }

    }

    return element;
}

pizza.prototype.show = function( element ) {
    element.style.transition = 'opacity 1s';
    element.style.opacity = '1';
}

pizza.prototype.hide = function( element ) {
    element.style.transition = 'opacity 0s';
    element.style.opacity = '0';
}

new pizza();