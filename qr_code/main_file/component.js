var component_creator = function(options) {
    this.id = options.id;
    this.width = options.width;
    this.height = options.height;
    this.container = options.container;
    this.data = options.data;
    this.root_path = '';
    this.render();
}

/**
 * Create element
 * @param 
    element_data
    {
        element_name,
        element_id,
        element_class,
        element_data
    } 
 * @return {element} element
 */
component_creator.prototype.create_element = function(element_data) {
    // If not have tag name exit
    if (!element_data.element_name) {
        return;
    }

    var element_name = element_data.element_name;
    var element_id = element_data.element_id;
    var element_class = element_data.element_class;
    var element_data = element_data.element_data ? element_data.element_data : [];
    var element_content = element_data.content ? element_data.content : '';

    // Create element
    var element = document.createElement(element_name);
    element.innerHTML = element_content;

    if (element_id) {
        element.setAttribute('id', element_id);
    }

    if (element_class) {
        element.setAttribute('class', element_class);
    }

    for (var key in element_data) {
        switch (key) {
            case 'event':
                // Adding event for element
                for (var key in element_data.event) {
                    if (typeof(element_data.event[key]) == 'function') {
                        element.addEventListener(key, element_data.event[key]);
                    }
                }

                break;

            case 'style':
                var style = '';

                for (var key in element_data.style) {
                    style = style + key.replace(/[A-Z]/g, function myFunction(chr) {
                        return '-' + chr.toLowerCase();
                    }) + ':' + element_data.style[key] + ';';
                }

                element.setAttribute('style', style);
                break;

            case 'child':
                for (var i = 0; i < element_data.child.length; i++) {
                    if (typeof(element_data.child[i]) == 'object') {
                        element.appendChild(element_data.child[i]);
                    }
                }

                break;

            case 'content':
                //igrone content
            break;

            default:
                element.setAttribute(key, element_data[key]);
            break;
        }

    }

    return element;
}

component_creator.prototype.hide_scene = function(scene) {
    scene.style.opacity = '0';
    setTimeout(function() {
        scene.style.zIndex = '-10';
    }, 500);
}

component_creator.prototype.load_js = function(link, callback) {
    if (!link) {
        return;
    }

    var is_loaded = document.querySelectorAll('script[src="' + link + '"]').length == 0 ? false : true;
    var have_callback = typeof(callback) == 'function' ? true : false;
    
    //fix_later
    is_loaded = false;
    
    //Check if script is loaded
    if (is_loaded) {
        if (have_callback) {
            callback();
        }
    } else {
        var script_element = document.createElement('script');

        script_element.setAttribute('src', link);
        script_element.setAttribute('async', true);
        script_element.onload = function() {
            if (have_callback) {
                callback();
            }
        }
    }

    document.querySelector('head').appendChild(script_element);
}

component_creator.prototype.load_css = function(link, callback) {
    if (!link) {
        return;
    }

    var style_element = document.createElement('link');
    style_element.setAttribute('href', link);
    style_element.setAttribute('type', 'text/css');
    style_element.setAttribute('rel', 'stylesheet');
    document.querySelector('head').appendChild(style_element);

    if (typeof(callback) == 'function') {
        callback();
    }
}

component_creator.prototype.render = function() {
    var _this = this;

    //-------------------------------------------
    // ELEMENT FOR FIRST SCENE
    //-------------------------------------------
    this.first_scene = {
        render: function() {
            this.claim_coupon_button = _this.create_element({
                element_name: 'div',
                element_class: 'coupon-button',
                element_data: {
                    style: {
                        background: _this.data.claimBtnBgColor,
                        bottom: '20px',
                        boxShadow: '0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24)',
                        color: _this.data.claimBtnTextColor ? _this.data.claimBtnTextColor: '#fff',
                        height: '37px',
                        lineHeight: '37px',
                        width: '80%',
                        position: 'absolute',
                        left: '50%',
                        transform: 'translateX(-50%)',
                        textAlign: 'center',
                        fontFamily: 'arial',
                        fontWeight: 'bold',
                        cursor: 'pointer'
                    },
                    content: _this.data.claimBtnText
                }
            });

            var first_scene_child = [];
            var add_claim_coupon_function = function(element) {
                element.addEventListener('click', function() {
                    _this.hide_scene(this.first_scene_element);
                    _this.load_js(_this.root_path + 'plugin/rasterizeHTML.allinone.js', function() {
                        var canvas_element = document.createElement('canvas');
                        var html = _this.wrapper_element.innerHTML;
                        canvas_element.setAttribute('width', _this.width);
                        canvas_element.setAttribute('height', _this.height);
                        canvas_element.setAttribute('style', 'display: none;');
                        document.body.appendChild(canvas_element);
                        rasterizeHTML.drawHTML(html, canvas_element).then(function (result) {
                            var sliceSize = 1024;
                            console.log(canvas_element.toDataURL());
                            var byteChars = window.atob(canvas_element.toDataURL().replace(/^data:image\/(png|jpg);base64,/, ""));
                            console.log('byteChars');
                            var byteArrays = [];
                            console.log('byteArrays');
                            // var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);
                            var isSafari = false;
                            for (var offset = 0, len = byteChars.length; offset < len; offset += sliceSize) {
                                var slice = byteChars.slice(offset, offset + sliceSize);

                                var byteNumbers = new Array(slice.length);
                                for (var i = 0; i < slice.length; i++) {
                                    byteNumbers[i] = slice.charCodeAt(i);
                                }

                                var byteArray = new Uint8Array(byteNumbers);
                                byteArrays.push(byteArray);
                            }
                            console.log('isSafari');
                            blobObj = new Blob(byteArrays, {type: 'image/png'});
                            _this.overlay.popup_send_button.addEventListener('click', function() {
                                var formData = new FormData();
                                formData.append('email', _this.overlay.popup_input.value);
                                formData.append('image', blobObj);
                                var xhttp = new XMLHttpRequest();
                                xhttp.open('POST', 'http://test.mobileads.com/api/coupon/mail-coupon');
                                xhttp.send(formData);
                                xhttp.onreadystatechange = function() { console.log(this); }
                            });
                            console.log(isSafari);
                            if(!isSafari) {
                                _this.overlay.popup_mailing_method_download_button.setAttribute('href', canvas_element.toDataURL());
                                _this.overlay.popup_mailing_method_download_button.setAttribute('download','coupon_image');
                            } else {
                                window.location.href = window.URL.createObjectURL(blobObj);
                                console.log(window.URL.createObjectURL(blobObj));
                            }
                        }, function (e) {
                            console.log('An error occured:', e);
                        });
                    });
                }.bind(this));
            }.bind(this);

            if (_this.data.showClaimBtn) {
                first_scene_child.push(this.claim_coupon_button);
            }

            this.first_scene_element = _this.create_element({
                element_name: 'div',
                element_class: 'first-scene wrapper',
                element_data: {
                    style: {
                        height: '100%',
                        width: '100%',
                        position: 'absolute',
                        background: 'url("'+ _this.data.couponBgImg + '")',
                        zIndex: '10',
                        transition: 'opacity 0.5s'
                    },
                    child: first_scene_child
                }
            });

            add_claim_coupon_function(_this.data.showClaimBtn ? this.claim_coupon_button : this.first_scene_element);

            return this.first_scene_element;
        }
    }
    //-------------------------------------------
    // END FIRST SCENE
    //-------------------------------------------

    //-------------------------------------------
    // ELEMENT FOR SECOND SCENE
    //-------------------------------------------
    this.second_scene = {
        render: function() {
            //Load plugins
            _this.load_css('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

            this.claim_coupon_button = _this.create_element({
                element_name: 'div',
                element_class: 'coupon-button',
                element_data: {
                    style: {
                        background: _this.data.couponCodeBgColor,
                        bottom: '20px',
                        boxShadow: '0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24)',
                        color: '#fff',
                        cursor: 'pointer',
                        height: '37px',
                        lineHeight: '37px',
                        marginTop: '10px',
                        padding: '0 5px',
                        textAlign: 'center',
                        fontFamily: 'arial',
                        fontWeight: 'bold',
                        width: '100%'
                    },
                    event: {
                        click: function() {
                            _this.overlay.show(_this.overlay.popup_mailing_method);
                        }
                    },
                    content: ( _this.data.couponCode ? 
                             _this.data.couponCode :
                             'Click to choice' )
                             +' <i class="fa fa-envelope-o" aria-hidden="true"></i>'
                }
            });
            
            this.qr_code = (function() {
                var element = _this.create_element({
                    element_name: 'div',
                    element_class: 'qr-code',
                    element_data: {
                        style: {
                            margin: '0 auto',
                            height: '98px',
                            width: '98px',
                        }
                    }
                });
                if( _this.data.conversionTracker ) {
                    _this.load_js(_this.root_path + 'plugin/qrcode.min.js', function() {
                        new QRCode(element, {
                            text: _this.data.conversionTracker,
                            width: 98,
                            height: 98
                        });
                    }); 
                }

                return element;
            })();

            _this.data.instructionTextColor = _this.data.instructionTextColor ? _this.data.instructionTextColor : '#000';
            var instruction_content = '<p style="margin: 0; color:' + _this.data.instructionTextColor + '">How to redeem</p>';
            _this.data.instructionText = _this.data.instructionText.split('|');
            for(var key in _this.data.instructionText) {
                var count = parseInt(key) + 1;
                instruction_content = instruction_content + '<p style="margin: 0;">' + count + '. ' + _this.data.instructionText[key] + '</p>';
            }

            this.content_left_container = _this.create_element({
                element_name: 'div',
                element_class: 'left-container',
                element_data: {
                    style: {
                        fontFamily: 'arial'
                    },
                    content: instruction_content,
                    child: [this.claim_coupon_button]
                }
            });

            this.content_right_container = _this.create_element({
                element_name: 'div',
                element_class: 'right-container',
                element_data: {
                    style: {
                        alignSelf: 'flex-end',
                        marginLeft: '2%',
                    },
                    child: !Boolean(_this.data.conversionTracker) ? [] : [this.qr_code]
                }
            });

            this.background = _this.create_element({
                element_name: 'img',
                element_class: 'background',
                element_data: {
                    style: {
                        position: 'absolute',
                        height: '100%',
                        width: '100%',
                        zIndex: '-1'
                    },
                    src: _this.data.pClaimBgImg
                }
            });

            this.content_container_element = _this.create_element({
                element_name: 'div',
                element_class: 'container',
                element_data: {
                    style: {
                        bottom: '10px',
                        display: 'flex',
                        justifyContent: 'space-around',
                        left: '50%',
                        padding: '5px',
                        position: 'absolute',
                        transform: 'translateX(-50%)',
                        width: '90%'
                    },
                    child: [
                        this.content_left_container,
                        this.content_right_container
                    ]
                }
            });

            this.second_scene_element = _this.create_element({
                element_name: 'div',
                element_class: 'second-scene wrapper',
                element_data: {
                    style: {
                        height: '100%',
                        position: 'absolute',
                        width: '100%'
                    },
                    child: [
                        this.content_container_element,
                        this.background
                    ]
                }
            });

            return this.second_scene_element;
        }
    }

    this.overlay = {
        render_popup: function() {
            this.popup_heading = _this.create_element({
                element_name: 'p',
                element_data: {
                    content: 'Send coupon to your email'
                }
            });

            this.popup_input = _this.create_element({
                element_name: 'input',
                element_data: {
                    style: {
                        border: '1px solid rgba(0, 0, 0, 0.31)',
                        display: 'block',
                        height: '30px',
                        margin: '0 auto',
                        paddingLeft: '2%',
                        width: '98%'
                    },
                    placeholder: 'mail@emobilecoupons.com',
                    type: 'text'
                }
            });

            this.popup_send_button = _this.create_element({
                element_name: 'a',
                element_data: {
                    style: {
                        background: '#5cb85c',
                        borderRadius: '6px',
                        color: 'white',
                        cursor: 'pointer',
                        display: 'block',
                        fontFamily: 'arial',
                        fontWeight: 'bold',
                        margin: '0 auto',
                        marginTop: '20px',
                        marginTop: '10px',
                        padding: '10px 0',
                        textAlign: 'center',
                        width: '80%'
                    },
                    content: 'Send Coupon'
                }
            });

            this.popup_close_icon = _this.create_element({
                element_name: 'i',
                element_class: 'fa fa-times-circle',
                element_data: {
                    style: {
                        color: '#ff5d55',
                        cursor: 'pointer',
                        float: 'right',
                        fontSize: '25px'
                    },
                    event: {
                        click: function() {
                            _this.overlay.hide(_this.overlay.popup_sending_email);
                        }
                    }
                }
            });

            this.popup_sending_email = _this.create_element({
                element_name: 'div',
                element_data: {
                    style: {
                        background: 'white',
                        fontFamily: 'arial',
                        left: '50%',
                        opacity: '0',
                        padding: '10px',
                        position: 'absolute',
                        top: '50%',
                        transform: 'translate( -50%, -50% )',
                        transition: 'opacity 0.5s',
                        width: '75%',
                        zIndex: '-10'
                    },
                    child: [
                        this.popup_close_icon,
                        this.popup_heading,
                        this.popup_input,
                        this.popup_send_button
                    ]
                }
            });

            return this.popup_sending_email;
        },
        render_overlay: function() {
            this.overlay_element = _this.create_element({
                element_name: 'div',
                element_class: 'overlay',
                element_data: {
                    style: {
                        background: '#000',
                        height: '100%',
                        opacity: '0',
                        position: 'absolute',
                        transition: 'opacity 1s',
                        width: '100%',
                        zIndex: '-10'
                    }
                }
            });

            return this.overlay_element;
        },
        render_mailing_method: function() {
            this.popup_mailing_close_icon = _this.create_element({
                element_name: 'i',
                element_class: 'fa fa-times-circle',
                element_data: {
                    style: {
                        color: '#ff5d55',
                        cursor: 'pointer',
                        float: 'right',
                        fontSize: '25px'
                    },
                    event: {
                        click: function() {
                            _this.overlay.hide(_this.overlay.popup_mailing_method);
                        }
                    }
                }
            });

            this.popup_mailing_method_download_button = _this.create_element({
                element_name: 'a',
                element_data: {
                    style: {
                        color: '#000',
                        cursor: 'pointer',
                        display: 'block',
                        padding: '5px 0',
                        textDecoration: 'none',
                        width: '100%'
                    },
                    event: {
                        click: function() {
                            _this.overlay.hide(_this.overlay.popup_mailing_method)
                        }
                    },
                    content: 'Download coupon'
                }
            });

            this.popup_mailing_method_email_button = _this.create_element({
                element_name: 'a',
                element_data: {
                    style: {
                        color: '#000',
                        cursor: 'pointer',
                        display: 'block',
                        padding: '5px 0',
                        textDecoration: 'none',
                        width: '100%'
                    },
                    event: {
                        click: function() {
                            _this.overlay.hide(_this.overlay.popup_mailing_method);
                            _this.overlay.show(_this.overlay.popup_sending_email);
                        }
                    },
                    content: 'Send email'
                }
            });
            var method = [this.popup_mailing_close_icon];
            if( _this.data.enableEmail ) {
                method.push( this.popup_mailing_method_download_button );
            }

            if( _this.data.enableScreenshot ) {
                method.push( this.popup_mailing_method_email_button );   
            }

            this.popup_mailing_method = _this.create_element({
                element_name: 'div',
                element_data: {
                    style: {
                        background: 'white',
                        fontFamily: 'arial',
                        left: '50%',
                        opacity: '0',
                        padding: '10px',
                        position: 'absolute',
                        top: '50%',
                        transform: 'translate( -50%, -50% )',
                        transition: 'opacity 0.5s',
                        width: '75%',
                        zIndex: '-10'
                    },
                    child: method
                }
            });

            return this.popup_mailing_method;
        },
        show: function(element) {
            this.overlay_element.style.opacity = '0.7';
            this.overlay_element.style.zIndex = '11';
            element.style.opacity = '1';
            element.style.zIndex = '12';
        },
        hide: function(element) {
            this.overlay_element.style.opacity = '0';
            this.overlay_element.style.zIndex = '-10';
            element.style.opacity = '0';
            element.style.zIndex = '-10';
        }
    }
    //-------------------------------------------
    // END SECOND SCENE
    //-------------------------------------------
    _this.wrapper_element = _this.create_element({
        element_name: 'div',
        element_id: _this.id,
        element_class: _this.id,
        element_data: {
            style: {
                height: _this.width,
                width: _this.height,
                position: 'relative',
                margin: '0 auto',
                overflow: 'hidden',
                boxShadow: '0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.24)'
            },
            child: [
                _this.first_scene.render(),
                _this.second_scene.render(),
                _this.overlay.render_overlay(),
                _this.overlay.render_popup(),
                _this.overlay.render_mailing_method()
            ]
        }
    });

    _this.container.appendChild(_this.wrapper_element);
}
