var component = function (options) {
    this.obj    = null;
	this.id 	= options.id;
	this.width  = options.width;
	this.height = options.height;
    this.ele    = document.querySelector(options.selector);

	this.type             = options.data.type;
	this.url              = options.data.url;
    this.showFriendsFaces = options.data.showFriendsFaces;
    this.adaptContainer   = options.data.adaptContainer;
    this.hideCoverPhoto   = options.data.hideCoverPhoto;
    this.useSmallHeader   = options.data.useSmallHeader;

    (options.data.messages) ? this.FBmessages = 'messages' : this.FBmessages = '';
    (options.data.events) ? this.FBevents = 'events' : this.FBevents = '';
    (options.data.timeline) ? this.FBtimeline = 'timeline' : this.FBtimeline = '';

	this.urlEncode();
	this.render();
	this.events();
}

component.prototype.urlEncode = function() {
    this.url = (this.url + '').toString();
    this.url = encodeURIComponent(this.url).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
}

component.prototype.render = function () {
	this.obj                 = document.createElement('div');
    this.obj.style.height    = this.height + 'px';
    this.obj.style.width     = this.width + 'px';
    this.obj.style.overflowY = 'scroll';

    if (this.type == 'post') {
        this.obj.innerHTML = '<iframe src="https://www.facebook.com/plugins/post.php?href='+this.url+'&width='+this.width+'&show_text=true&appId=337960779915361&height='+this.height+'" width="'+this.width+'" height="100%" style="border:none;overflow-y:scroll" frameborder="0" allowTransparency="true"></iframe>';
        this.ele.appendChild(this.obj);
    } else if (this.type == 'page') {
        this.ele.innerHTML = '<iframe src="https://www.facebook.com/plugins/page.php?href='+this.url+'&tabs='+this.FBtimeline+'%2C%20'+this.FBevents+'%2C%20'+this.FBmessages+'&width='+this.width+'&height='+this.height+'&small_header='+this.useSmallHeader+'&adapt_container_width='+this.adaptContainer+'&hide_cover='+this.hideCoverPhoto+'&show_facepile='+this.showFriendsFaces+'&appId=337960779915361" width="'+this.width+'" height="'+this.height+'" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>';
    }
}

component.prototype.events = function () {
    var id = this.id; // specify the value to a variable before using.

    this.obj.addEventListener('click', function (event) {
        alert('item ' + id);
    });
}
